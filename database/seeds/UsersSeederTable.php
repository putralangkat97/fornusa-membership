<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;

class UsersSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $authorRole = Role::where('name', 'author')->first();

        $author = User::create([
            'name' => 'Super Admin',
            'email' => 'super@admin.com',
            'username' => 'superadmin',
            'password' => bcrypt('superadmin')
        ]);

        $author->roles()->attach($authorRole);
    }
}
