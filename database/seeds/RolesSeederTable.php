<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        // Author
        Role::create(['name' => 'author']);
        
        // Admin
        Role::create(['name' => 'asahan', 'nama_kota'               => '01 - Kabupaten Asahan']);
        Role::create(['name' => 'batubara', 'nama_kota'             => '02 - Kabupaten Batu Bara']);
        Role::create(['name' => 'dairi', 'nama_kota'                => '03 - Kabupaten Dairi']);
        Role::create(['name' => 'deliserdang', 'nama_kota'          => '04 - Deli Serdang']);
        Role::create(['name' => 'humbanghasundutan', 'nama_kota'    => '05 - Humbang Hasundutan']);
        Role::create(['name' => 'karo', 'nama_kota'                 => '06 - Karo']);
        Role::create(['name' => 'labuhanbatu', 'nama_kota'          => '07 - Labuhan Batu']);
        Role::create(['name' => 'labuhanbatuselatan', 'nama_kota'   => '08 - Labuhan Batu Selatan']);
        Role::create(['name' => 'labuhanbatuutara', 'nama_kota'     => '09 - Labuhan Batu Utara']);
        Role::create(['name' => 'langkat', 'nama_kota'              => '10 - Langkat']);
        Role::create(['name' => 'mandailingnatal', 'nama_kota'      => '11 - Mandailing Natal']);
        Role::create(['name' => 'nias', 'nama_kota'                 => '12 - Nias']);
        Role::create(['name' => 'niasbarat', 'nama_kota'            => '13 - Nias Barat']);
        Role::create(['name' => 'niasselatan', 'nama_kota'          => '14 - Nias Selatan']);
        Role::create(['name' => 'niasutara', 'nama_kota'            => '15 - Nias Utara']);
        Role::create(['name' => 'padanglawas', 'nama_kota'          => '16 - Padang Lawas']);
        Role::create(['name' => 'padanglawasutara', 'nama_kota'     => '17 - Padang Lawas Utara']);
        Role::create(['name' => 'pakpakbharat', 'nama_kota'         => '18 - Pakpak Bharat']);
        Role::create(['name' => 'samosir', 'nama_kota'              => '19 - Samosir']);
        Role::create(['name' => 'serdangbedagai', 'nama_kota'       => '20 - Serdang Bedagai']);
        Role::create(['name' => 'simalungun', 'nama_kota'           => '21 - Simalungun']);
        Role::create(['name' => 'tapanuliselatan', 'nama_kota'      => '22 - Tapanuli Selatan']);
        Role::create(['name' => 'tapanulitengah', 'nama_kota'       => '23 - Tapanuli Tengah']);
        Role::create(['name' => 'tapanuliutara', 'nama_kota'        => '24 - Tapanuli Utara']);
        Role::create(['name' => 'tobasamosir', 'nama_kota'          => '25 - Toba Samosir']);
        Role::create(['name' => 'binjai', 'nama_kota'               => '26 - Binjai']);
        Role::create(['name' => 'gunungsitoli', 'nama_kota'         => '27 - Gunungsitoli']);
        Role::create(['name' => 'medan', 'nama_kota'                => '28 - Medan']);
        Role::create(['name' => 'padangsidempuan', 'nama_kota'      => '29 - Padangsidempuan']);
        Role::create(['name' => 'pematangsiantar', 'nama_kota'      => '30 - Pematangsiantar']);
        Role::create(['name' => 'sibolga', 'nama_kota'              => '31 - Sibolga']);
        Role::create(['name' => 'tanjungbalai', 'nama_kota'         => '32 - Tanjung Balai']);
        Role::create(['name' => 'tebingtinggi', 'nama_kota'         => '33 - Tebing Tinggi']);
    }
}
