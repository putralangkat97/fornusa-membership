<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTobasamosirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tobasamosirs', function (Blueprint $table) {
            $table->id();
            $table->string('no_anggota', 20);
            $table->string('nama', 100);
            $table->string('jenis_kelamin', 20);
            $table->string('asal_sekolah', 100);
            $table->string('tempat_lahir', 100);
            $table->string('tanggal_lahir', 50);
            $table->string('golongan_darah', 5);
            $table->string('lead_train_satu', 50)->nullable();
            $table->string('lead_train_dua', 50)->nullable();
            $table->string('lead_train_tiga', 50)->nullable();
            $table->text('alamat');
            $table->string('no_telepon', 20);
            $table->string('nama_mentor', 100);
            $table->string('nama_kelompok', 100)->nullable();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tobasamosirs');
    }
}
