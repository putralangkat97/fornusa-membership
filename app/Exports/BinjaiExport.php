<?php

namespace App\Exports;

use App\Models\Binjai;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Reader\Xls\Style\Border;

class BinjaiExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Binjai::select('no_anggota', 'nama', 'asal_sekolah', 'jenis_kelamin', 'tanggal_lahir', 'no_telepon', 'alamat', 'lead_train_satu', 'lead_train_dua', 'lead_train_tiga', 'nama_mentor')->get();
    }

    public function headings(): array {
        return [
            'No. Anggota',
            'Nama',
            'Asal Sekolah',
            'L/P',
            'Tgl. Lahir',
            'No. Telp',
            'Alamat',
            'LT 1',
            'LT 2',
            'LT 3',
            'Mentor'
        ];
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:'.$event->sheet->getDelegate()->getHighestColumn().'1';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                
                // Apply array of styles to B2:G8 cell range
                $styleArray = array(
                    'borders' => array(
                        'allBorders' => array(
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '2d2d2d'],
                        )
                    )
                );
                $col = $event->sheet->getDelegate()->getHighestColumn();
                $row = $event->sheet->getDelegate()->getHighestRow();
                $event->sheet->getDelegate()->getStyle('A1:'.$col.$row)->applyFromArray($styleArray);

                // Set first row to height 20
                 $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(26);
            },
        ];
    }
}
