<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pematangsiantar extends Model
{
    protected $fillable = [
        'no_anggota',
        'nama',
        'nama_mentor',
        'nama_kelompok',
        'asal_sekolah',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'golongan_darah',
        'no_telepon',
        'alamat',
        'created_by',
        'lead_train_satu',
        'lead_train_dua',
        'lead_train_tiga',
    ];
}
