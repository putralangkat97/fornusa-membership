<?php

namespace App\Http\Controllers\Author;

use App\User;
use Validator;
use App\Models\Karo;
use App\Models\Nias;
use App\Models\Role;
use App\Models\Dairi;
use App\Models\Medan;
use App\Models\Asahan;
use App\Models\Binjai;
use App\Models\Mentor;
use App\Models\Langkat;
use App\Models\Samosir;
use App\Models\Sibolga;
use App\Models\Batubara;
use App\Models\Niasbarat;
use App\Models\Niasutara;
use App\Models\Simalungun;
use App\Models\Deliserdang;
use App\Models\Labuhanbatu;
use App\Models\Niasselatan;
use App\Models\Padanglawas;
use App\Models\Tobasamosir;
use App\Models\Gunungsitoli;
use App\Models\Pakpakbharat;
use App\Models\Tanjungbalai;
use App\Models\Tebingtinggi;
use Illuminate\Http\Request;
use App\Models\Tapanuliutara;
use App\Models\Serdangbedagai;
use App\Models\Tapanulitengah;
use App\Models\Mandailingnatal;
use App\Models\Padangsidempuan;
use App\Models\Pematangsiantar;
use App\Models\Tapanuliselatan;
use App\Models\Labuhanbatuutara;
use App\Models\Padanglawasutara;
use App\Models\Humbanghasundutan;
use App\Models\Labuhanbatuselatan;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Total semua anggota
        $asahanAll = Asahan::all()->count();
        $batubaraAll = Batubara::all()->count();
        $dairiAll = Dairi::all()->count();
        $deliserdangAll = Deliserdang::all()->count();
        $humbanghasundutanAll = Humbanghasundutan::all()->count();
        $karoAll = Karo::all()->count();
        $labuhanbatuAll = Labuhanbatu::all()->count();
        $labuhanbatuselatanAll = Labuhanbatuselatan::all()->count();
        $labuhanbatuutaraAll = Labuhanbatuutara::all()->count();
        $langkatAll = Langkat::all()->count();
        $mandailingnatalAll = Mandailingnatal::all()->count();
        $niasAll = Nias::all()->count();
        $niasbaratAll = Niasbarat::all()->count();
        $niasselatanAll = Niasselatan::all()->count();
        $niasutaraAll = Niasutara::all()->count();
        $padanglawasAll = Padanglawas::all()->count();
        $padanglawasutaraAll = Padanglawasutara::all()->count();
        $pakpakbharatAll = Pakpakbharat::all()->count();
        $samosirAll = Samosir::all()->count();
        $serdangbedagaiAll = Serdangbedagai::all()->count();
        $simalungunAll = Simalungun::all()->count();
        $tapanuliselatanAll = Tapanuliselatan::all()->count();
        $tapanulitengahAll = Tapanulitengah::all()->count();
        $tapanuliutaraAll = Tapanuliutara::all()->count();
        $tobasamosirAll = Tobasamosir::all()->count();
        $binjaiAll = Binjai::all()->count();
        $gunungsitoliAll = Gunungsitoli::all()->count();
        $medanAll = Medan::all()->count();
        $padangsidempuanAll = Padangsidempuan::all()->count();
        $pematangsiantarAll = Pematangsiantar::all()->count();
        $sibolgaAll = Sibolga::all()->count();
        $tanjungbalaiAll = Tanjungbalai::all()->count();
        $tebingtinggiAll = Tebingtinggi::all()->count();
        $total = $asahanAll + $batubaraAll + $dairiAll + $deliserdangAll + $humbanghasundutanAll + $karoAll + $labuhanbatuAll + $labuhanbatuselatanAll + $labuhanbatuutaraAll + $langkatAll + $mandailingnatalAll + $niasAll + $niasbaratAll + $niasselatanAll + $niasutaraAll + $padanglawasAll + $padanglawasutaraAll + $pakpakbharatAll + $samosirAll + $serdangbedagaiAll + $simalungunAll + $tapanuliselatanAll + $tapanulitengahAll + $tapanuliutaraAll + $tobasamosirAll + $binjaiAll + $gunungsitoliAll + $medanAll + $padangsidempuanAll + $pematangsiantarAll + $sibolgaAll + $tanjungbalaiAll + $tebingtinggiAll;

        // total semua anggota LT 1
        $asahanlt1 = Asahan::where('lead_train_satu', '!=', null)->get()->count();
        $batubaralt1 = Batubara::where('lead_train_satu', '!=', null)->get()->count();
        $dairilt1 = Dairi::where('lead_train_satu', '!=', null)->get()->count();
        $deliserdanglt1 = Deliserdang::where('lead_train_satu', '!=', null)->get()->count();
        $humbanghasundutanlt1 = Humbanghasundutan::where('lead_train_satu', '!=', null)->get()->count();
        $karolt1 = Karo::where('lead_train_satu', '!=', null)->get()->count();
        $labuhanbatult1 = Labuhanbatu::where('lead_train_satu', '!=', null)->get()->count();
        $labuhanbatuselatanlt1 = Labuhanbatuselatan::where('lead_train_satu', '!=', null)->get()->count();
        $labuhanbatuutaralt1 = Labuhanbatuutara::where('lead_train_satu', '!=', null)->get()->count();
        $langkatlt1 = Langkat::where('lead_train_satu', '!=', null)->get()->count();
        $mandailingnatallt1 = Mandailingnatal::where('lead_train_satu', '!=', null)->get()->count();
        $niaslt1 = Nias::where('lead_train_satu', '!=', null)->get()->count();
        $niasbaratlt1 = Niasbarat::where('lead_train_satu', '!=', null)->get()->count();
        $niasselatanlt1 = Niasselatan::where('lead_train_satu', '!=', null)->get()->count();
        $niasutaralt1 = Niasutara::where('lead_train_satu', '!=', null)->get()->count();
        $padanglawaslt1 = Padanglawas::where('lead_train_satu', '!=', null)->get()->count();
        $padanglawasutaralt1 = Padanglawasutara::where('lead_train_satu', '!=', null)->get()->count();
        $pakpakbharatlt1 = Pakpakbharat::where('lead_train_satu', '!=', null)->get()->count();
        $samosirlt1 = Samosir::where('lead_train_satu', '!=', null)->get()->count();
        $serdangbedagailt1 = Serdangbedagai::where('lead_train_satu', '!=', null)->get()->count();
        $simalungunlt1 = Simalungun::where('lead_train_satu', '!=', null)->get()->count();
        $tapanuliselatanlt1 = Tapanuliselatan::where('lead_train_satu', '!=', null)->get()->count();
        $tapanulitengahlt1 = Tapanulitengah::where('lead_train_satu', '!=', null)->get()->count();
        $tapanuliutaralt1 = Tapanuliutara::where('lead_train_satu', '!=', null)->get()->count();
        $tobasamosirlt1 = Tobasamosir::where('lead_train_satu', '!=', null)->get()->count();
        $binjailt1 = Binjai::where('lead_train_satu', '!=', null)->get()->count();
        $gunungsitolilt1 = Gunungsitoli::where('lead_train_satu', '!=', null)->get()->count();
        $medanlt1 = Medan::where('lead_train_satu', '!=', null)->get()->count();
        $padangsidempuanlt1 = Padangsidempuan::where('lead_train_satu', '!=', null)->get()->count();
        $pematangsiantarlt1 = Pematangsiantar::where('lead_train_satu', '!=', null)->get()->count();
        $sibolgalt1 = Sibolga::where('lead_train_satu', '!=', null)->get()->count();
        $tanjungbalailt1 = Tanjungbalai::where('lead_train_satu', '!=', null)->get()->count();
        $tebingtinggilt1 = Tebingtinggi::where('lead_train_satu', '!=', null)->get()->count();
        $totallt1 = $asahanlt1 + $batubaralt1 + $dairilt1 + $deliserdanglt1 + $humbanghasundutanlt1 + $karolt1 + $labuhanbatult1 + $labuhanbatuselatanlt1 + $labuhanbatuutaralt1 + $langkatlt1 + $mandailingnatallt1 + $niaslt1 + $niasbaratlt1 + $niasselatanlt1 + $niasutaralt1 + $padanglawaslt1 + $padanglawasutaralt1 + $pakpakbharatlt1 + $samosirlt1 + $serdangbedagailt1 + $simalungunlt1 + $tapanuliselatanlt1 + $tapanulitengahlt1 + $tapanuliutaralt1 + $tobasamosirlt1 + $binjailt1 + $gunungsitolilt1 + $medanlt1 + $padangsidempuanlt1 + $pematangsiantarlt1 + $sibolgalt1 + $tanjungbalailt1 + $tebingtinggilt1;

        // Total semua anggota LT 2
        $asahanlt2 = Asahan::where('lead_train_dua', '!=', null)->get()->count();
        $batubaralt2 = Batubara::where('lead_train_dua', '!=', null)->get()->count();
        $dairilt2 = Dairi::where('lead_train_dua', '!=', null)->get()->count();
        $deliserdanglt2 = Deliserdang::where('lead_train_dua', '!=', null)->get()->count();
        $humbanghasundutanlt2 = Humbanghasundutan::where('lead_train_dua', '!=', null)->get()->count();
        $karolt2 = Karo::where('lead_train_dua', '!=', null)->get()->count();
        $labuhanbatult2 = Labuhanbatu::where('lead_train_dua', '!=', null)->get()->count();
        $labuhanbatuselatanlt2 = Labuhanbatuselatan::where('lead_train_dua', '!=', null)->get()->count();
        $labuhanbatuutaralt2 = Labuhanbatuutara::where('lead_train_dua', '!=', null)->get()->count();
        $langkatlt2 = Langkat::where('lead_train_dua', '!=', null)->get()->count();
        $mandailingnatallt2 = Mandailingnatal::where('lead_train_dua', '!=', null)->get()->count();
        $niaslt2 = Nias::where('lead_train_dua', '!=', null)->get()->count();
        $niasbaratlt2 = Niasbarat::where('lead_train_dua', '!=', null)->get()->count();
        $niasselatanlt2 = Niasselatan::where('lead_train_dua', '!=', null)->get()->count();
        $niasutaralt2 = Niasutara::where('lead_train_dua', '!=', null)->get()->count();
        $padanglawaslt2 = Padanglawas::where('lead_train_dua', '!=', null)->get()->count();
        $padanglawasutaralt2 = Padanglawasutara::where('lead_train_dua', '!=', null)->get()->count();
        $pakpakbharatlt2 = Pakpakbharat::where('lead_train_dua', '!=', null)->get()->count();
        $samosirlt2 = Samosir::where('lead_train_dua', '!=', null)->get()->count();
        $serdangbedagailt2 = Serdangbedagai::where('lead_train_dua', '!=', null)->get()->count();
        $simalungunlt2 = Simalungun::where('lead_train_dua', '!=', null)->get()->count();
        $tapanuliselatanlt2 = Tapanuliselatan::where('lead_train_dua', '!=', null)->get()->count();
        $tapanulitengahlt2 = Tapanulitengah::where('lead_train_dua', '!=', null)->get()->count();
        $tapanuliutaralt2 = Tapanuliutara::where('lead_train_dua', '!=', null)->get()->count();
        $tobasamosirlt2 = Tobasamosir::where('lead_train_dua', '!=', null)->get()->count();
        $binjailt2 = Batubara::where('lead_train_dua', '!=', null)->get()->count();
        $gunungsitolilt2 = Gunungsitoli::where('lead_train_dua', '!=', null)->get()->count();
        $medanlt2 = Medan::where('lead_train_dua', '!=', null)->get()->count();
        $padangsidempuanlt2 = Padangsidempuan::where('lead_train_dua', '!=', null)->get()->count();
        $pematangsiantarlt2 = Pematangsiantar::where('lead_train_dua', '!=', null)->get()->count();
        $sibolgalt2 = Sibolga::where('lead_train_dua', '!=', null)->get()->count();
        $tanjungbalailt2 = Tanjungbalai::where('lead_train_dua', '!=', null)->get()->count();
        $tebingtinggilt2 = Tebingtinggi::where('lead_train_dua', '!=', null)->get()->count();
        $totallt2 = $asahanlt2 + $batubaralt2 + $dairilt2 + $deliserdanglt2 + $humbanghasundutanlt2 + $karolt2 + $labuhanbatult2 + $labuhanbatuselatanlt2 + $labuhanbatuutaralt2 + $langkatlt2 + $mandailingnatallt2 + $niaslt2 + $niasbaratlt2 + $niasselatanlt2 + $niasutaralt2 + $padanglawaslt2 + $padanglawasutaralt2 + $pakpakbharatlt2 + $samosirlt2 + $serdangbedagailt2 + $simalungunlt2 + $tapanuliselatanlt2 + $tapanulitengahlt2 + $tapanuliutaralt2 + $tobasamosirlt2 + $binjailt2 + $gunungsitolilt2 + $medanlt2 + $padangsidempuanlt2 + $pematangsiantarlt2 + $sibolgalt2 + $tanjungbalailt2 + $tebingtinggilt2;

        // Total semua anggota LT 3
        $asahanlt3 = Asahan::where('lead_train_tiga', '!=', null)->get()->count();
        $batubaralt3 = Batubara::where('lead_train_tiga', '!=', null)->get()->count();
        $dairilt3 = Dairi::where('lead_train_tiga', '!=', null)->get()->count();
        $deliserdanglt3 = Deliserdang::where('lead_train_tiga', '!=', null)->get()->count();
        $humbanghasundutanlt3 = Humbanghasundutan::where('lead_train_tiga', '!=', null)->get()->count();
        $karolt3 = Karo::where('lead_train_tiga', '!=', null)->get()->count();
        $labuhanbatult3 = Labuhanbatu::where('lead_train_tiga', '!=', null)->get()->count();
        $labuhanbatuselatanlt3 = Labuhanbatuselatan::where('lead_train_tiga', '!=', null)->get()->count();
        $labuhanbatuutaralt3 = Labuhanbatuutara::where('lead_train_tiga', '!=', null)->get()->count();
        $langkatlt3 = Langkat::where('lead_train_tiga', '!=', null)->get()->count();
        $mandailingnatallt3 = Mandailingnatal::where('lead_train_tiga', '!=', null)->get()->count();
        $niaslt3 = Nias::where('lead_train_tiga', '!=', null)->get()->count();
        $niasbaratlt3 = Niasbarat::where('lead_train_tiga', '!=', null)->get()->count();
        $niasselatanlt3 = Niasselatan::where('lead_train_tiga', '!=', null)->get()->count();
        $niasutaralt3 = Niasutara::where('lead_train_tiga', '!=', null)->get()->count();
        $padanglawaslt3 = Padanglawas::where('lead_train_tiga', '!=', null)->get()->count();
        $padanglawasutaralt3 = Padanglawasutara::where('lead_train_tiga', '!=', null)->get()->count();
        $pakpakbharatlt3 = Pakpakbharat::where('lead_train_tiga', '!=', null)->get()->count();
        $samosirlt3 = Samosir::where('lead_train_tiga', '!=', null)->get()->count();
        $serdangbedagailt3 = Serdangbedagai::where('lead_train_tiga', '!=', null)->get()->count();
        $simalungunlt3 = Simalungun::where('lead_train_tiga', '!=', null)->get()->count();
        $tapanuliselatanlt3 = Tapanuliselatan::where('lead_train_tiga', '!=', null)->get()->count();
        $tapanulitengahlt3 = Tapanulitengah::where('lead_train_tiga', '!=', null)->get()->count();
        $tapanuliutaralt3 = Tapanuliutara::where('lead_train_tiga', '!=', null)->get()->count();
        $tobasamosirlt3 = Tobasamosir::where('lead_train_tiga', '!=', null)->get()->count();
        $binjailt3 = Binjai::where('lead_train_tiga', '!=', null)->get()->count();
        $gunungsitolilt3 = Gunungsitoli::where('lead_train_tiga', '!=', null)->get()->count();
        $medanlt3 = Medan::where('lead_train_tiga', '!=', null)->get()->count();
        $padangsidempuanlt3 = Padangsidempuan::where('lead_train_tiga', '!=', null)->get()->count();
        $pematangsiantarlt3 = Pematangsiantar::where('lead_train_tiga', '!=', null)->get()->count();
        $sibolgalt3 = Sibolga::where('lead_train_tiga', '!=', null)->get()->count();
        $tanjungbalailt3 = Tanjungbalai::where('lead_train_tiga', '!=', null)->get()->count();
        $tebingtinggilt3 = Tebingtinggi::where('lead_train_tiga', '!=', null)->get()->count();
        $totallt3 = $asahanlt3 + $batubaralt3 + $dairilt3 + $deliserdanglt3 + $humbanghasundutanlt3 + $karolt3 + $labuhanbatult3 + $labuhanbatuselatanlt3 + $labuhanbatuutaralt3 + $langkatlt3 + $mandailingnatallt3 + $niaslt3 + $niasbaratlt3 + $niasselatanlt3 + $niasutaralt3 + $padanglawaslt3 + $padanglawasutaralt3 + $pakpakbharatlt3 + $samosirlt3 + $serdangbedagailt3 + $simalungunlt3 + $tapanuliselatanlt3 + $tapanulitengahlt3 + $tapanuliutaralt3 + $tobasamosirlt3 + $binjailt3 + $gunungsitolilt3 + $medanlt3 + $padangsidempuanlt3 + $pematangsiantarlt3 + $sibolgalt3 + $tanjungbalailt3 + $tebingtinggilt3;

        // Total anggota per wilayah

        return view('author.index')->with([
            'total' => $total,
            'lt1' => $totallt1,
            'lt2' => $totallt2,
            'lt3' => $totallt3,
            'asahan' => $asahanAll,
            'asahanlt1' => $asahanlt1,
            'asahanlt2' => $asahanlt2,
            'asahanlt3' => $asahanlt3,
            'batubara' => $batubaraAll,
            'batubaralt1' => $batubaralt1,
            'batubaralt2' => $batubaralt2,
            'batubaralt3' => $batubaralt3,
            'dairi' => $dairiAll,
            'dairilt1' => $dairilt1,
            'dairilt2' => $dairilt2,
            'dairilt3' => $dairilt3,
            'deliserdang' => $deliserdangAll,
            'deliserdanglt1' => $deliserdanglt1,
            'deliserdanglt2' => $deliserdanglt2,
            'deliserdanglt3' => $deliserdanglt3,
            'humbanghasundutan' => $humbanghasundutanAll,
            'humbanghasundutanlt1' => $humbanghasundutanlt1,
            'humbanghasundutanlt2' => $humbanghasundutanlt2,
            'humbanghasundutanlt3' => $humbanghasundutanlt3,
            'karo' => $karoAll,
            'karolt1' => $karolt1,
            'karolt2' => $karolt2,
            'karolt3' => $karolt3,
            'labuhanbatu' => $labuhanbatuAll,
            'labuhanbatult1' => $labuhanbatult1,
            'labuhanbatult2' => $labuhanbatult2,
            'labuhanbatult3' => $labuhanbatult3,
            'labuhanbatuselatan' => $labuhanbatuselatanAll,
            'labuhanbatuselatanlt1' => $labuhanbatuselatanlt1,
            'labuhanbatuselatanlt2' => $labuhanbatuselatanlt2,
            'labuhanbatuselatanlt3' => $labuhanbatuselatanlt3,
            'labuhanbatuutara' => $labuhanbatuutaraAll,
            'labuhanbatuutaralt1' => $labuhanbatuutaralt1,
            'labuhanbatuutaralt2' => $labuhanbatuutaralt2,
            'labuhanbatuutaralt3' => $labuhanbatuutaralt3,
            'langkat' => $langkatAll,
            'langkatlt1' => $langkatlt1,
            'langkatlt2' => $langkatlt2,
            'langkatlt3' => $langkatlt3,
            'mandailingnatal' => $mandailingnatalAll,
            'mandailingnatallt1' => $mandailingnatallt1,
            'mandailingnatallt2' => $mandailingnatallt2,
            'mandailingnatallt3' => $mandailingnatallt3,
            'nias' => $niasAll,
            'niaslt1' => $niaslt1,
            'niaslt2' => $niaslt2,
            'niaslt3' => $niaslt3,
            'niasbarat' => $niasbaratAll,
            'niasbaratlt1' => $niasbaratlt1,
            'niasbaratlt2' => $niasbaratlt2,
            'niasbaratlt3' => $niasbaratlt3,
            'niasselatan' => $niasselatanAll,
            'niasselatanlt1' => $niasselatanlt1,
            'niasselatanlt2' => $niasselatanlt2,
            'niasselatanlt3' => $niasselatanlt3,
            'niasutara' => $niasutaraAll,
            'niasutaralt1' => $niasutaralt1,
            'niasutaralt2' => $niasutaralt2,
            'niasutaralt3' => $niasutaralt3,
            'padanglawas' => $padanglawasAll,
            'padanglawaslt1' => $padanglawaslt1,
            'padanglawaslt2' => $padanglawaslt2,
            'padanglawaslt3' => $padanglawaslt3,
            'padanglawasutara' => $padanglawasutaraAll,
            'padanglawasutaralt1' => $padanglawasutaralt1,
            'padanglawasutaralt2' => $padanglawasutaralt2,
            'padanglawasutaralt3' => $padanglawasutaralt3,
            'pakpakbharat' => $pakpakbharatAll,
            'pakpakbharatlt1' => $pakpakbharatlt1,
            'pakpakbharatlt2' => $pakpakbharatlt2,
            'pakpakbharatlt3' => $pakpakbharatlt3,
            'samosir' => $samosirAll,
            'samosirlt1' => $samosirlt1,
            'samosirlt2' => $samosirlt2,
            'samosirlt3' => $samosirlt3,
            'serdangbedagai' => $serdangbedagaiAll,
            'serdangbedagailt1' => $serdangbedagailt1,
            'serdangbedagailt2' => $serdangbedagailt2,
            'serdangbedagailt3' => $serdangbedagailt3,
            'simalungun' => $simalungunAll,
            'simalungunlt1' => $simalungunlt1,
            'simalungunlt2' => $simalungunlt2,
            'simalungunlt3' => $simalungunlt3,
            'tapanuliselatan' => $tapanuliselatanAll,
            'tapanuliselatanlt1' => $tapanuliselatanlt1,
            'tapanuliselatanlt2' => $tapanuliselatanlt2,
            'tapanuliselatanlt3' => $tapanuliselatanlt3,
            'tapanulitengah' => $tapanulitengahAll,
            'tapanulitengahlt1' => $tapanulitengahlt1,
            'tapanulitengahlt2' => $tapanulitengahlt2,
            'tapanulitengahlt3' => $tapanulitengahlt3,
            'tapanuliutara' => $tapanuliutaraAll,
            'tapanuliutaralt1' => $tapanuliutaralt1,
            'tapanuliutaralt2' => $tapanuliutaralt2,
            'tapanuliutaralt3' => $tapanuliutaralt3,
            'tobasamosir' => $tobasamosirAll,
            'tobasamosirlt1' => $tobasamosirlt1,
            'tobasamosirlt2' => $tobasamosirlt2,
            'tobasamosirlt3' => $tobasamosirlt3,
            'binjai' => $binjaiAll,
            'binjailt1' => $binjailt1,
            'binjailt2' => $binjailt2,
            'binjailt3' => $binjailt3,
            'gunungsitoli' => $gunungsitoliAll,
            'gunungsitolilt1' => $gunungsitolilt1,
            'gunungsitolilt2' => $gunungsitolilt2,
            'gunungsitolilt3' => $gunungsitolilt3,
            'medan' => $medanAll,
            'medanlt1' => $medanlt1,
            'medanlt2' => $medanlt2,
            'medanlt3' => $medanlt3,
            'padangsidempuan' => $padangsidempuanAll,
            'padangsidempuanlt1' => $padangsidempuanlt1,
            'padangsidempuanlt2' => $padangsidempuanlt2,
            'padangsidempuanlt3' => $padangsidempuanlt3,
            'pematangsiantar' => $pematangsiantarAll,
            'pematangsiantarlt1' => $pematangsiantarlt1,
            'pematangsiantarlt2' => $pematangsiantarlt2,
            'pematangsiantarlt3' => $pematangsiantarlt3,
            'sibolga' => $sibolgaAll,
            'sibolgalt1' => $sibolgalt1,
            'sibolgalt2' => $sibolgalt2,
            'sibolgalt3' => $sibolgalt3,
            'tanjungbalai' => $tanjungbalaiAll,
            'tanjungbalailt1' => $tanjungbalailt1,
            'tanjungbalailt2' => $tanjungbalailt2,
            'tanjungbalailt3' => $tanjungbalailt3,
            'tebingtinggi' => $tebingtinggiAll,
            'tebingtinggilt1' => $tebingtinggilt1,
            'tebingtinggilt2' => $tebingtinggilt2,
            'tebingtinggilt3' => $tebingtinggilt3,
        ]);
    }

    public function mentor() {
        $data = Mentor::all();
        return view('author.mentor')->with(['mentor' => $data]);
    }

    public function editMentor($id) {
        $data = Mentor::findOrFail($id);
        return response()->json(['result' => $data]);
    }

    public function addMentor(Request $request) {
        $rules = array('name' => 'required');

        $error = Validator::make($request->all(), $rules);

        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form = array('name' => $request->name);

        Mentor::create($form);

        return response()->json(['success' => 'Data berhasil ditambahkan']);
    }

    public function updateMentor(Request $request, Mentor $mentor) {
        $rules = array('name' => 'required');

        $error = Validator::make($request->all(), $rules);

        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form = array('name' => $request->name);

        Mentor::whereId($request->hidden_id)->update($form);

        return response()->json(['success' => 'Data berhasil diupdate']);
    }

    public function deleteMentor($id) {
        $data = Mentor::where('id', $id)->delete();

        if($data = 1) {
        $success = true;
        $message = "Data deleted successfully";
        } else {
        $success = true;
        $message = "Data not found!";
        }

        return response()->json(['success' => $success, 'message' => $message]);
    }

    public function admin()
    {
        $role = Role::where('name', '!=', 'author')->get();
        $user = User::where('id', '!=', 1)->get();
        return view('author.admin')->with(['admin' => $user, 'roles' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'username' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role,
        );

        $adminRole = Role::where('name', $request->role)->first();

        $admin = User::create($form_data);

        $admin->roles()->attach($adminRole);

        return response()->json(['success' => 'Data Added']);
    }

    /**
     * Role a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rules(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'name' => $request->name,
        );

        $role = Role::create($form_data);

        return response()->json(['success' => 'Data Added']);
    }
}
