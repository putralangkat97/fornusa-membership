<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username() {
        return 'username';
    }

    public function redirectTo() {
        if (Auth::user()->hasAnyRole('author')) {
            $this->redirectTo = route('author.index');
            return $this->redirectTo;
        }
        
        else if (Auth::user()->hasAnyRole('asahan')) {
            $this->redirectTo = route('asahan.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('batubara')) {
            $this->redirectTo = route('batubara.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('dairi')) {
            $this->redirectTo = route('dairi.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('deliserdang')) {
            $this->redirectTo = route('deliserdang.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('humbanghasundutan')) {
            $this->redirectTo = route('humbanghasundutan.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('karo')) {
            $this->redirectTo = route('karo.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('labuhanbatu')) {
            $this->redirectTo = route('labuhanbatu.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('labuhanbatuselatan')) {
            $this->redirectTo = route('labuhanbatuselatan.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('labuhanbatuutara')) {
            $this->redirectTo = route('labuhanbatuutara.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('langkat')) {
            $this->redirectTo = route('langkat.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('mandailingnatal')) {
            $this->redirectTo = route('mandailingnatal.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('nias')) {
            $this->redirectTo = route('nias.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('niasbarat')) {
            $this->redirectTo = route('niasbarat.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('niasselatan')) {
            $this->redirectTo = route('niasselatan.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('niasutara')) {
            $this->redirectTo = route('niasutara.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('padanglawas')) {
            $this->redirectTo = route('padanglawas.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('padanglawasutara')) {
            $this->redirectTo = route('padanglawasutara.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('pakpakbharat')) {
            $this->redirectTo = route('pakpakbharat.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('samosir')) {
            $this->redirectTo = route('samosir.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('serdangbedagai')) {
            $this->redirectTo = route('serdangbedagai.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('simalungun')) {
            $this->redirectTo = route('simalungun.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('tapanuliselatan')) {
            $this->redirectTo = route('tapanuliselatan.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('tapanulitengah')) {
            $this->redirectTo = route('tapanulitengah.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('tapanuliutara')) {
            $this->redirectTo = route('tapanuliutara.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('tobasamosir')) {
            $this->redirectTo = route('tobasamosir.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('binjai')) {
            $this->redirectTo = route('binjai.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('gunungsitoli')) {
            $this->redirectTo = route('gunungsitoli.dashboard');
            return $this->redirectTo;
        }

        else if (Auth::user()->hasAnyRole('medan')) {
            $this->redirectTo = route('medan.dashboard');
            return $this->redirectTo;
        }
        
        else if (Auth::user()->hasAnyRole('padangsidempuan')) {
            $this->redirectTo = route('padangsidempuan.dashboard');
            return $this->redirectTo;
        }
        
        else if (Auth::user()->hasAnyRole('pematangsiantar')) {
            $this->redirectTo = route('pematangsiantar.dashboard');
            return $this->redirectTo;
        }
        
        else if (Auth::user()->hasAnyRole('sibolga')) {
            $this->redirectTo = route('sibolga.dashboard');
            return $this->redirectTo;
        }
        
        else if (Auth::user()->hasAnyRole('tanjungbalai')) {
            $this->redirectTo = route('tanjungbalai.dashboard');
            return $this->redirectTo;
        }
        
        else if (Auth::user()->hasAnyRole('tebingtinggi')) {
            $this->redirectTo = route('tebingtinggi.dashboard');
            return $this->redirectTo;
        }
    }
}
