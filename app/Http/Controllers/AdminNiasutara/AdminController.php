<?php

namespace App\Http\Controllers\AdminNiasutara;

use PDF;
use Validator;
use Carbon\Carbon;
use App\Models\Mentor;
use App\Models\Niasutara;
use Illuminate\Http\Request;
use App\Exports\AnggotaExport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function index() {
        $data = Niasutara::all()->count();
        $ltsatu = Niasutara::where('lead_train_satu', '!=', null)->get()->count();
        $ltdua = Niasutara::where('lead_train_dua', '!=', null)->get()->count();
        $lttiga = Niasutara::where('lead_train_tiga', '!=', null)->get()->count();
        return view('admin.niasutara.dashboard')->with([
            'total' => $data,
            'lt1' => $ltsatu,
            'lt2' => $ltdua,
            'lt3' => $lttiga
        ]);
    }

    public function all() {
        $lastNumber = Niasutara::orderBy('id', 'desc')->first()->no_anggota ?? 0;

        $lastIncrement = substr($lastNumber, -3);

        $newNumber = '15'.date('Y').str_pad($lastIncrement + 1, 3, 0, STR_PAD_LEFT);

        $mentor = Mentor::all();
        $member = Niasutara::all();

        return view('admin.niasutara._all')->with(['data' => $mentor, 'number' => $newNumber, 'member' => $member]);
    }

    public function add(Request $request) {
        $rules = array(
            'no_anggota' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'asal_sekolah' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'golongan_darah' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'nama_mentor' => 'required'
        );
      
        $error = Validator::make($request->all(), $rules);
      
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
      
        $form = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'asal_sekolah' => $request->asal_sekolah,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'golongan_darah' => $request->golongan_darah,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
            'nama_mentor' => $request->nama_mentor,
            'nama_kelompok' => $request->nama_kelompok,
            'created_by' => Auth::user()->username,
        );
      
        Niasutara::create($form);
    
        return response()->json(['success' => 'Data anggota berhasil di tambah']);
    }

    // Edit
    public function edit($id) {
        $data = Niasutara::findOrFail($id);
        return response()->json(['result' => $data]);
    }

    public function update(Request $request) {
        $rules = array(
            'no_anggota' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'asal_sekolah' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'golongan_darah' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'nama_mentor' => 'required'
        );
      
        $error = Validator::make($request->all(), $rules);
      
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
      
        $form = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'asal_sekolah' => $request->asal_sekolah,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'golongan_darah' => $request->golongan_darah,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
            'nama_mentor' => $request->nama_mentor,
            'nama_kelompok' => $request->nama_kelompok,
            'created_by' => Auth::user()->username,
        );
      
        Niasutara::whereId($request->hidden_id)->update($form);
    
        return response()->json(['success' => 'Data anggota berhasil di update']);
    }

    public function destroy($id) {
        $data = Niasutara::where('id', $id)->delete();

        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }

        return response()->json(['success' => $success, 'message' => $message]);
    }
    
    public function cancelLt1($id) {
        $data = Niasutara::where('id', $id)->update(['lead_train_satu' => null]);
    
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }
    
        return response()->json(['success' => $success, 'message' => $message]);
    }
    
    public function cancelLt2($id) {
        $data = Niasutara::where('id', $id)->update(['lead_train_dua' => null]);
    
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }
    
        return response()->json(['success' => $success, 'message' => $message]);
    }
    
    public function cancelLt3($id) {
        $data = Niasutara::where('id', $id)->update(['lead_train_tiga' => null]);
    
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }
    
        return response()->json(['success' => $success, 'message' => $message]);
    }

    // LT 1
    public function ltsatu($id) {
        $data = Niasutara::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    public function ltsatuTambah(Request $request, Niasutara $ltsatu) {
        $rules = array(
            'lead_train_satu' => 'required',
        );
      
        $error = Validator::make($request->all(), $rules);
    
        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
    
        $form = array(
            'lead_train_satu' => $request->lead_train_satu,
            'created_by' => Auth::user()->username,
        );
    
        Niasutara::whereId($request->hidden_ids)->update($form);
    
        return response()->json(['success' => 'LT 1 berhasil ditambahkan']);
    }

    // LT 2
    public function ltdua($id) {
        $data = Niasutara::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    public function ltduaTambah(Request $request, Niasutara $ltdua) {
        $rules = array(
            'lead_train_dua' => 'required',
        );
      
        $error = Validator::make($request->all(), $rules);
    
        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
    
        $form = array(
            'lead_train_dua' => $request->lead_train_dua,
            'created_by' => Auth::user()->username,
        );
    
        Niasutara::whereId($request->hidden_idss)->update($form);
    
        return response()->json(['success' => 'LT 1 berhasil ditambahkan']);
    }

    // LT 3
    public function lttiga($id) {
        $data = Niasutara::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    public function lttigaTambah(Request $request, Niasutara $lttiga) {
        $rules = array(
            'lead_train_tiga' => 'required',
        );
      
        $error = Validator::make($request->all(), $rules);
    
        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
    
        $form = array(
            'lead_train_tiga' => $request->lead_train_tiga,
            'created_by' => Auth::user()->username,
        );
    
        Niasutara::whereId($request->hidden_idsss)->update($form);
    
        return response()->json(['success' => 'LT 1 berhasil ditambahkan']);
    }

    public function lt1() {
        $member = Niasutara::where('lead_train_satu', '!=', null)->where('lead_train_dua', '=', null)->get();
        return view('admin.niasutara._lt1')->with('member', $member);
    }

    public function lt2() {
        $member = Niasutara::where('lead_train_dua', '!=', null)->where('lead_train_tiga', '=', null)->get();
        return view('admin.niasutara._lt2')->with('member', $member);
    }

    public function lt3() {
        $member = Niasutara::where('lead_train_tiga', '!=', null)->get();
        return view('admin.niasutara._lt3')->with('member', $member);
    }

    public function pdfgenerate() {
        $data = Niasutara::all();
        $pdf = PDF::loadView('pdfview', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->download();
    }

    public function pdfprint() {
        $data = Niasutara::all();
        $pdf = PDF::loadView('pdfview', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function excel() {
        $user = Auth::user()->username;
        $date = Carbon::now('Asia/Jakarta')->format('dmY');
        return Excel::download(new AnggotaExport, $user.'_'.$date.'.xlsx');
    }

    public function card($id) {
        $data = Niasutara::findOrFail($id);
        return view('admin.niasutara.card', compact('data'));
    }
}
