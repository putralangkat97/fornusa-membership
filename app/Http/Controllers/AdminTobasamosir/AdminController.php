<?php

namespace App\Http\Controllers\AdminTobasamosir;

use PDF;
use Validator;
use Carbon\Carbon;
use App\Models\Mentor;
use App\Models\Tobasamosir;
use Illuminate\Http\Request;
use App\Exports\AnggotaExport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function index() {
        $data = Tobasamosir::all()->count();
        $ltsatu = Tobasamosir::where('lead_train_satu', '!=', null)->get()->count();
        $ltdua = Tobasamosir::where('lead_train_dua', '!=', null)->get()->count();
        $lttiga = Tobasamosir::where('lead_train_tiga', '!=', null)->get()->count();
        return view('admin.tobasamosir.dashboard')->with([
            'total' => $data,
            'lt1' => $ltsatu,
            'lt2' => $ltdua,
            'lt3' => $lttiga
        ]);
    }

    public function all() {
        $lastNumber = Tobasamosir::orderBy('id', 'desc')->first()->no_anggota ?? 0;

        $lastIncrement = substr($lastNumber, -3);

        $newNumber = '25'.date('Y').str_pad($lastIncrement + 1, 3, 0, STR_PAD_LEFT);

        $mentor = Mentor::all();
        $member = Tobasamosir::all();

        return view('admin.tobasamosir._all')->with(['data' => $mentor, 'number' => $newNumber, 'member' => $member]);
    }

    public function add(Request $request) {
        $rules = array(
            'no_anggota' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'asal_sekolah' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'golongan_darah' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'nama_mentor' => 'required'
        );
      
        $error = Validator::make($request->all(), $rules);
      
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
      
        $form = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'asal_sekolah' => $request->asal_sekolah,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'golongan_darah' => $request->golongan_darah,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
            'nama_mentor' => $request->nama_mentor,
            'nama_kelompok' => $request->nama_kelompok,
            'created_by' => Auth::user()->username,
        );
      
        Tobasamosir::create($form);
    
        return response()->json(['success' => 'Data anggota berhasil di tambah']);
    }

    // Edit
    public function edit($id) {
        $data = Tobasamosir::findOrFail($id);
        return response()->json(['result' => $data]);
    }

    public function update(Request $request) {
        $rules = array(
            'no_anggota' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required',
            'asal_sekolah' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'golongan_darah' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'nama_mentor' => 'required'
        );
      
        $error = Validator::make($request->all(), $rules);
      
        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
      
        $form = array(
            'no_anggota' => $request->no_anggota,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'asal_sekolah' => $request->asal_sekolah,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'golongan_darah' => $request->golongan_darah,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
            'nama_mentor' => $request->nama_mentor,
            'nama_kelompok' => $request->nama_kelompok,
            'created_by' => Auth::user()->username,
        );
      
        Tobasamosir::whereId($request->hidden_id)->update($form);
    
        return response()->json(['success' => 'Data anggota berhasil di update']);
    }

    public function destroy($id) {
        $data = Tobasamosir::where('id', $id)->delete();

        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }

        return response()->json(['success' => $success, 'message' => $message]);
    }
    
    public function cancelLt1($id) {
        $data = Tobasamosir::where('id', $id)->update(['lead_train_satu' => null]);
    
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }
    
        return response()->json(['success' => $success, 'message' => $message]);
    }
    
    public function cancelLt2($id) {
        $data = Tobasamosir::where('id', $id)->update(['lead_train_dua' => null]);
    
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }
    
        return response()->json(['success' => $success, 'message' => $message]);
    }
    
    public function cancelLt3($id) {
        $data = Tobasamosir::where('id', $id)->update(['lead_train_tiga' => null]);
    
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully!";
        } else {
            $success = true;
            $message = "Data not found";
        }
    
        return response()->json(['success' => $success, 'message' => $message]);
    }

    // LT 1
    public function ltsatu($id) {
        $data = Tobasamosir::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    public function ltsatuTambah(Request $request, Tobasamosir $ltsatu) {
        $rules = array(
            'lead_train_satu' => 'required',
        );
      
        $error = Validator::make($request->all(), $rules);
    
        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
    
        $form = array(
            'lead_train_satu' => $request->lead_train_satu,
            'created_by' => Auth::user()->username,
        );
    
        Tobasamosir::whereId($request->hidden_ids)->update($form);
    
        return response()->json(['success' => 'LT 1 berhasil ditambahkan']);
    }

    // LT 2
    public function ltdua($id) {
        $data = Tobasamosir::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    public function ltduaTambah(Request $request, Tobasamosir $ltdua) {
        $rules = array(
            'lead_train_dua' => 'required',
        );
      
        $error = Validator::make($request->all(), $rules);
    
        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
    
        $form = array(
            'lead_train_dua' => $request->lead_train_dua,
            'created_by' => Auth::user()->username,
        );
    
        Tobasamosir::whereId($request->hidden_idss)->update($form);
    
        return response()->json(['success' => 'LT 1 berhasil ditambahkan']);
    }

    // LT 3
    public function lttiga($id) {
        $data = Tobasamosir::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    public function lttigaTambah(Request $request, Tobasamosir $lttiga) {
        $rules = array(
            'lead_train_tiga' => 'required',
        );
      
        $error = Validator::make($request->all(), $rules);
    
        if($error->fails()) {
            return response()->json(['errors' => $error->errors()->all()]);
        }
    
        $form = array(
            'lead_train_tiga' => $request->lead_train_tiga,
            'created_by' => Auth::user()->username,
        );
    
        Tobasamosir::whereId($request->hidden_idsss)->update($form);
    
        return response()->json(['success' => 'LT 1 berhasil ditambahkan']);
    }

    public function lt1() {
        $member = Tobasamosir::where('lead_train_satu', '!=', null)->where('lead_train_dua', '=', null)->get();
        return view('admin.tobasamosir._lt1')->with('member', $member);
    }

    public function lt2() {
        $member = Tobasamosir::where('lead_train_dua', '!=', null)->where('lead_train_tiga', '=', null)->get();
        return view('admin.tobasamosir._lt2')->with('member', $member);
    }

    public function lt3() {
        $member = Tobasamosir::where('lead_train_tiga', '!=', null)->get();
        return view('admin.tobasamosir._lt3')->with('member', $member);
    }

    public function pdfgenerate() {
        $data = Tobasamosir::all();
        $pdf = PDF::loadView('pdfview', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->download();
    }

    public function pdfprint() {
        $data = Tobasamosir::all();
        $pdf = PDF::loadView('pdfview', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function excel() {
        $user = Auth::user()->username;
        $date = Carbon::now('Asia/Jakarta')->format('dmY');
        return Excel::download(new AnggotaExport, $user.'_'.$date.'.xlsx');
    }

    public function card($id) {
        $data = Tobasamosir::findOrFail($id);
        return view('admin.tobasamosir.card', compact('data'));
    }
}
