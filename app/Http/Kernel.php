<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'auth.author' => \App\Http\Middleware\AuthorMiddleware::class,
        'auth.asahan' => \App\Http\Middleware\AdminAsahan::class,
        'auth.batubara' => \App\Http\Middleware\AdminBatubara::class,
        'auth.dairi' => \App\Http\Middleware\AdminDairi::class,
        'auth.deliserdang' => \App\Http\Middleware\AdminDeliserdang::class,
        'auth.humbanghasundutan' => \App\Http\Middleware\AdminHumbanghasundutan::class,
        'auth.karo' => \App\Http\Middleware\AdminKaro::class,
        'auth.labuhanbatu' => \App\Http\Middleware\AdminLabuhanbatu::class,
        'auth.labuhanbatuselatan' => \App\Http\Middleware\AdminLabuhanbatuselatan::class,
        'auth.labuhanbatuutara' => \App\Http\Middleware\AdminLabuhanbatuutara::class,
        'auth.langkat' => \App\Http\Middleware\AdminLangkat::class,
        'auth.mandailingnatal' => \App\Http\Middleware\AdminMandailingnatal::class,
        'auth.nias' => \App\Http\Middleware\AdminNias::class,
        'auth.niasbarat' => \App\Http\Middleware\AdminNiasbarat::class,
        'auth.niasselatan' => \App\Http\Middleware\AdminNiasselatan::class,
        'auth.niasutara' => \App\Http\Middleware\AdminNiasutara::class,
        'auth.padanglawas' => \App\Http\Middleware\AdminPadanglawas::class,
        'auth.padanglawasutara' => \App\Http\Middleware\AdminPadanglawasutara::class,
        'auth.pakpakbharat' => \App\Http\Middleware\AdminPakpakbharat::class,
        'auth.samosir' => \App\Http\Middleware\AdminSamosir::class,
        'auth.serdangbedagai' => \App\Http\Middleware\AdminSerdangbedagai::class,
        'auth.simalungun' => \App\Http\Middleware\AdminSimalungun::class,
        'auth.tapanuliselatan' => \App\Http\Middleware\AdminTapanuliselatan::class,
        'auth.tapanulitengah' => \App\Http\Middleware\AdminTapanulitengah::class,
        'auth.tapanuliutara' => \App\Http\Middleware\AdminTapanuliutara::class,
        'auth.tobasamosir' => \App\Http\Middleware\AdminTobasamosir::class,
        'auth.binjai' => \App\Http\Middleware\AdminBinjai::class,
        'auth.gunungsitoli' => \App\Http\Middleware\AdminGunungsitoli::class,
        'auth.medan' => \App\Http\Middleware\AdminMedan::class,
        'auth.padangsidempuan' => \App\Http\Middleware\AdminPadangsidempuan::class,
        'auth.pematangsiantar' => \App\Http\Middleware\AdminPematangsiantar::class,
        'auth.sibolga' => \App\Http\Middleware\AdminSibolga::class,
        'auth.tanjungbalai' => \App\Http\Middleware\AdminTanjungbalai::class,
        'auth.tebingtinggi' => \App\Http\Middleware\AdminTebingtinggi::class,
    ];
}
