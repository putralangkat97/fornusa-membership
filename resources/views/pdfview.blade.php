<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PDF</title>
</head>
<style>
  * {
    font-family: sans-serif;
  }

  table {
      border-collapse: collapse;
      margin: 0;
      width: 100%;
  }

  td, tr, th {
      padding: 10px;
      border: 1px solid #2d2d2d;
      text-align: center;
  }

  th {
      background-color: #64c5b1;
      color: #fff;
  }

  h1 {
    
  }
</style>
<body>
  <h1>Data Anggota FORNUSA</h1>
  <table>
    <thead>
      <tr>
        <th>#</th>
        <th>No. Anggota</th>
        <th>Nama Lengkap</th>
        <th>Asal Sekolah</th>
        <th>L/P</th>
        <th>Tgl. Lahir</th>
        <th>LT 1</th>
        <th>LT 2</th>
        <th>LT 3</th>
        <th>Nama Mentor</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $key => $d)
      <tr>
        <td>{{ ++$key }}</td>
        <td>{{ $d->no_anggota }}</td>
        <td>{{ $d->nama }}</td>
        <td>{{ $d->asal_sekolah }}</td>
        <td>{{ $d->jenis_kelamin }}</td>
        <td>{{ $d->tanggal_lahir }}</td>
        <td>
          @if($d->lead_train_satu !== null)
            <strong style="color: forestgreen;"><span>{{ $d->lead_train_satu }}</span></strong>
          @else
          <strong style="color: orange;">Belum</strong>
          @endif
        </td>
        <td>
          @if($d->lead_train_dua !== null)
            <strong style="color: forestgreen;"><span>{{ $d->lead_train_dua }}</span></strong>
          @else
          <strong style="color: orange;">Belum</strong>
          @endif
        </td>
        <td>
          @if($d->lead_train_tiga !== null)
            <strong style="color: forestgreen;"><span>{{ $d->lead_train_tiga }}</span></strong>
          @else
          <strong style="color: orange;">Belum</strong>
          @endif
        </td>
        <td>{{ $d->nama_mentor }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>