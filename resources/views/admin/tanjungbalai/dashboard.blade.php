@extends('layouts.master')
@section('title')
Tanjungbalai | {{ Auth::user()->username }}
@endsection

@section('content')
<h1>Dashboard</h1>

<div class="row">
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                  <i class="fe-users avatar-title font-30 text-white"></i>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total Anggota</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $total }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-danger widget-two-icon align-self-center">
                  <span class="avatar-title font-30 text-white">LT1</span>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total LT 1</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $lt1 - $lt2 }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-info widget-two-icon align-self-center">
                  <span class="avatar-title font-30 text-white">LT2</span>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total LT 2</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $lt2 - $lt3 }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-success widget-two-icon align-self-center">
                  <span class="avatar-title font-30 text-white">LT3</span>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total LT 3</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $lt3 }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
</div>
<!-- end row -->
@endsection