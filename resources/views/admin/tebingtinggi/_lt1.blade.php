@extends('layouts.master')
@section('title')
LT 1 | {{ Auth::user()->username }}
@endsection

@section('content')
<div class="card-box table-responsive">
    <div class="btn-group" role="group" aria-label="Basic example">
      <a href="{{ route('tebingtinggi.all') }}" class="btn btn-light">Semua Data</a>
      <a href="{{ route('tebingtinggi.ltsatu') }}" class="btn btn-primary">LT 1</a>
      <a href="{{ route('tebingtinggi.ltdua') }}" class="btn btn-light">LT 2</a>
      <a href="{{ route('tebingtinggi.lttiga') }}" class="btn btn-light">LT 3</a>
    </div>
<hr>
    
    <br>
    <h4 class="header-title">Data Anggota LT 1</h4>

    <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead class="bg-primary text-light">
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">No. Anggota</th>
                <th class="text-center">Nama Lengkap</th>
                <th class="text-center">JK</th>
                <th class="text-center">Tgl. Lahir</th>
                <th class="text-center">LT 1</th>
                <th class="text-center">Create by</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody class="text-center">
        @foreach($member as $key => $row)
            <tr>
               <td>{{ ++$key }}</td>
               <td>{{ $row->no_anggota }}</td>
               <td>{{ $row->nama }}</td>
               <td>{{ $row->jenis_kelamin }}</td>
               <td>{{ $row->tanggal_lahir }}</td>
               <td><i class="fa fa-check text-success"></i> {{ $row->lead_train_satu }}</td>
               <td>{{ $row->created_by }}</td>
               <td>
                  <a href="#" class="ltsatu btn btn-sm btn-info" id="{{ $row->id }}"><i class="fa fa-edit"></i> Edit</a>
                  <a href="#" class="cancel btn btn-sm btn-warning" onclick="cancelConfirmation({{ $row->id }});"><i class="fa fa-times"></i> Batal</a>
               </td>
            </tr>
         @endforeach
        </tbody>
    </table>
</div>

<!-- Modals -->
@include('partials.modals._lt1')

<script>
$(document).ready(function() {
  
    // Datepicker Add LT 1
    $('.datepickerssss').datepicker({ uiLibrary: 'bootstrap', format: 'dd mmmm yyyy' });
    $(document).on('click', '.ltsatu', function() {
      var id = $(this).attr('id');
      $('#form_results').html('');
      $.ajax({
        url: '/tebingtinggi/'+id+'/lt1',
        dataType: 'json',
        success: function(data) {
          $('#formModals').modal('show');
          $('#action_buttons').val('Update LT 1');
          $('#actions').val('Update');
          $('#lead_train_satus').val(data.data.lead_train_satu);
          $('#hidden_ids').val(data.data.id);
        }
      });
    });

    $('#sample_forms').on('submit', function(event) {
      event.preventDefault();
      var id = $(this).attr('id');

      $.ajax({
        url: "{{ route('tebingtinggi.ltsatuTambah') }}",
        method: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
          var html = '';
          if (data.errors) {
            html = '<div class="alert alert-danger">';
            for (var count = 0; count < data.errors.length; count++) {
                html += '<li><b>'+data.errors[count]+'</b></li>';
            }
            html += '</div>';
          }

          if (data.success) {
            Swal.fire({
              type: 'success',
              icon: 'success',
              title: 'Berhasil',
              text: 'Data LT 1 berhasil ditambah'
            }).then(function() {
              location.reload();
            });
            $('#formModals').modal('hide');
            // html = '<div class="alert alert-success">'+data.success+'</div>';
            $('#sample_forms')[0].reset();
          }
          $('#form_results').html(html);
        }
      });
    });
  });
  
  function cancelConfirmation(id) {
    Swal.fire({
      icon: 'warning',
      title: "Batalkan?",
      text: "Apakah anda yakin ingin membatalkan Lead Train 3?",
      type: "warning",
      showCancelButton: !0,
      confirmButtonText: "Yes, Batalkan!",
      cancelButtonText: "Tidak!",
      reverseButtons: !0
    }).then(function (event) {
      if (event.value === true) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
          type: 'POST',
          url: '/tebingtinggi/cancel/'+id+'/lt1',
          data: { _token: CSRF_TOKEN },
          dataType: 'JSON',
          success: function (results) {
            if (results.success === true) {
              Swal.fire({
                toast: false,
                icon: 'success',
                title: "Success",
                text: "Data Deleted",
                showConfirmButton: true,
                // timer: 1500
              }).then(function() {
                location.href = '{{ URL("/tebingtinggi/all") }}';
              });
            } else {
              Swal.fire("Error!", results.message, "error");
            }
          }
        });
      } else {
        e.dismiss;
      }
    }, function (dismiss) {
      return false;
    });
  }
</script>

@endsection