@extends('layouts.partials.card')

@section('content')
<link href="https://fonts.googleapis.com/css2?family=Abel&display=swap" rel="stylesheet"> 
<style>

   .class {
      margin-top: 10px;
   }

html, body {
  background: #fff;
  font-family: Abel, Arial, Verdana, sans-serif;
}

.general h2 {
  margin-top: 20px;
}

.center {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

.card {
}

.card h1 {
  text-align: center;
}

.card .additional {
  position: absolute;
  width: 230px;
  height: 100%;
  transition: width 0.4s;
  overflow: hidden;
  z-index: 2;
}

.card .additional .user-card {
  width: 100%;
  height: 100%;
  position: relative;
  float: left;
  border-radius: 0 5px 5px 0;
}

.card .additional .user-card .points {
  top: 75%;
  width: 18em;
  text-align: center;
  font-size: 16px;
  text-transform: uppercase;
  color: white;
}

.card .additional .user-card img {
  top: 40%;
}

.card .additional .more-info {
  width: 30px;
  float: left;
  position: absolute;
  left: 150px;
  height: 100%;
}

.card .additional .more-info h1 {
  color: #fff;
  margin-bottom: 0;
}

.card .additional .coords {
  margin: 0 1rem;
  color: #fff;
  font-size: 1rem;
}

.card .additional .coords span + span {
  float: right;
}

.card .additional .stats {
  font-size: 2rem;
  display: flex;
  position: absolute;
  bottom: 1rem;
  left: 1rem;
  right: 1rem;
  top: auto;
  color: #fff;
}

.card .additional .stats > div {
  flex: 1;
  text-align: center;
}

.card .additional .stats i {
  display: block;
}

.card .additional .stats div.title {
  font-size: 0.75rem;
  font-weight: bold;
  text-transform: uppercase;
}

.card .additional .stats div.value {
  font-size: 1.5rem;
  font-weight: bold;
  line-height: 1.5rem;
}

.card .additional .stats div.value.infinity {
  font-size: 2.5rem;
}

@media print {
  .additional {
    background-color: #fa8072;
  }
}

.card .general {
  width: 350px;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1;
  box-sizing: border-box;
  padding: 1rem;
  padding-top: 0;
}

.card .general .more {
  position: absolute;
  bottom: 1rem;
  right: 1rem;
  font-size: 0.9em;
}

</style>
<div class="center">

  <div class="card" style="width: 580px;height: 330px;background-color: #fff;box-shadow: 0 8px 16px -8px rgba(0,0,0,0.4);border-radius: 6px;overflow: hidden;position: relative;margin: 1.5rem;">
    <div class="additional" style="background-color: #fa8072;">
      <div class="user-card">
        <div class="level center">
        </div>
        <div class="points center">
          <span>{{ $data['nama'] }}</span>
        </div>
        @if($data['jenis_kelamin'] == 'L')
        <img src="{{ asset('akhi.png') }}" alt="{{ $data['nama'] }}" width="150" height="150" class="center">
        @elseif($data['jenis_kelamin'] == 'P')
        <img src="{{ asset('ukhti.png') }}" alt="{{ $data['nama'] }}" width="150" height="150" class="center">
        @endif
      </div>
    </div>
    <div class="general" style="background-color: #fafafa;">
        <h2>{{ $data['no_anggota'] }}/MEDAN</h2>
        <div class="class">
        <div class="coords">
          <span>Lahir: {{ $data['tempat_lahir'] }}, {{ $data['tanggal_lahir'] }}</span>
        </div>
        <div class="coords">
          <span>Asal Sekolah: {{ $data['asal_sekolah'] }}</span>
        </div>
        <div class="coords">
          <span>JK: 
          @if($data['jenis_kelamin'] == 'L')
            Laki-Laki
          @elseif($data['jenis_kelamin'] == 'P')
            Perempuan
          @endif
          </span>
        </div>
        <div class="coords">
          <span>Mentor: {{ $data['nama_mentor'] }}</span>
        </div>
        @if($data['nama_kelompok'] !== null)
        <div class="coords">
          <span>Kelompok: {{ $data['nama_kelompok'] }}</span>
        </div>
        @else
        <div class="coords">
          <span>Kelompok: -</span>
        </div>
        @endif
        <div class="coords">
          <span>Gol. Darah: {{ $data['golongan_darah'] }}</span>
        </div>
        <div class="coords">
          <span>No. Telp: {{ $data['no_telepon'] }}</span>
        </div>
        <div class="coords">
          <span>Alamat: {{ $data['alamat'] }}</span>
        </div>
        <div class="coords">
          <span>Member Sejak: {{ date('d F Y', strtotime($data['created_at'])) }}</span>
        </div>
        </div>
    </div>
  </div>
</div>
@endsection