@extends('layouts.master')
@section('title')
Binjai | Setting
@endsection

@section('content')
    <div class="card-box">
        <h3>Ganti Password</h3>
        <button id="pass" class="btn btn-primary">Ganti</button>
    </div>

    @include('partials.modals._cpass')
@endsection