@extends('layouts.master')
@section('title')
Tapanuli Utara | {{ Auth::user()->username }}
@endsection

@section('content')
<div class="card-box table-responsive">
  <div class="btn-group" role="group" aria-label="Basic example">
    <a href="{{ route('tapanuliutara.all') }}" class="btn btn-primary">Semua Data</a>
    <a href="{{ route('tapanuliutara.ltsatu') }}" class="btn btn-light">LT 1</a>
    <a href="{{ route('tapanuliutara.ltdua') }}" class="btn btn-light">LT 2</a>
    <a href="{{ route('tapanuliutara.lttiga') }}" class="btn btn-light">LT 3</a>
  </div>

  <hr>
  <br>
  
  <h4 class="header-title">Data LT 1</h4>

  <div class="btn-group sub-header" role="group" aria-label="Basic example">
    <button type="button" name="tambah_anggota" id="tambah_anggota" class="btn btn-success btn-sm text-white"><i class="fa fa-plus"></i> Tambah Anggota</button>
    &nbsp;
    <a href="{{ route('tapanuliutara.excel') }}" class="btn btn-md btn-primary"><i class="fa fa-file-excel"></i> Excel</a>
    <a href="{{ route('tapanuliutara.pdf') }}" target="_blank" class="btn btn-md btn-danger"><i class="fa fa-file-pdf"></i> PDF</a>
    <a href="{{ route('tapanuliutara.print') }}" target="_blank" class="btn btn-md btn-secondary"><i class="fa fa-print"></i> Print</a>
  </div>

  <!-- Start Table -->
  <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    <thead class="bg-primary text-light">
      <tr>
        <th class="text-center">#</th>
        <th class="text-center">No. Anggota</th>
        <th class="text-center">Nama Lengkap</th>
        <th class="text-center">JK</th>
        <th class="text-center">Tgl. Lahir</th>
        <th class="text-center">LT 1</th>
        <th class="text-center">LT 2</th>
        <th class="text-center">LT 3</th>
        <th class="text-center">Create by</th>
        <th class="text-center">Action</th>

      </tr>
    </thead>
    <tbody>
      @foreach($member as $key => $m)
      <tr>
        <td class="text-center">{{ ++$key }}</td>
        <td class="text-center">{{ $m->no_anggota }}</td>
        <td class="text-center">{{ $m->nama }}</td>
        <td class="text-center">{{ $m->jenis_kelamin }}</td>
        <td class="text-center">{{ $m->tanggal_lahir }}</td>
        <td class="text-center">
          @if($m->lead_train_satu !== null)
          <strong><span class="text-success"><i class="fa fa-check"></i> {{ $m->lead_train_satu }}</span></strong>
          @else
          <strong><a href="#" class="ltsatu" id="{{ $m->id }}"><span class="badge badge-danger"><i class="fa fa-times"></i> belum</span></a></strong>
          @endif
        </td>
        <td class="text-center">
          @if($m->lead_train_dua !== null)
          <strong><span class="text-success"><i class="fa fa-check"></i> {{ $m->lead_train_dua }}</span></strong>
          @elseif($m->lead_train_satu == null)
          <strong><a href="#" onclick="alertsatu();"><span class="badge badge-danger"><i class="fa fa-times"></i> belum</span></a></strong>
          @else
          <strong><a href="#" class="ltdua" id="{{ $m->id }}"><span class="badge badge-danger"><i class="fa fa-times"></i> belum</span></a></strong>
          @endif
        </td>
        <td class="text-center">
          @if($m->lead_train_tiga !== null)
          <strong></span> <span class="text-success"><i class="fa fa-check"></i> {{ $m->lead_train_tiga }}</span></strong>
          @elseif($m->lead_train_dua == null)
          <strong><a href="#" onclick="alertdua();"><span class="badge badge-danger"><i class="fa fa-times"></i> belum</span></a></strong>
          @else
          <strong><a href="#" class="ltiga" id="{{ $m->id }}"><span class="badge badge-danger"><i class="fa fa-times"></i> belum</span></a></strong>
          @endif
        </td>
        <td class="text-center">{{ $m->created_by }}</td>
        <td class="text-center">
          <a href="#" name="edit" class="edit btn btn-sm btn-info" id="{{ $m->id }}"><i class="fa fa-edit"></i></a>
          &nbsp;|&nbsp;
          <a href="#" class="delete btn btn-sm btn-danger" onclick="deleteConfirmation({{ $m->id }})"><i class="fa fa-trash"></i></a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <!-- End Table -->
</div>

<!-- Modals -->
@include('partials.modals._add_edit')
@include('partials.modals._lt1')
@include('partials.modals._lt2')
@include('partials.modals._lt3')
<!-- End Modals -->

<!-- Script -->
<script>
  $(document).ready(function() {
    $('#datatable').DataTable();
    // Datepicker Add Member
    $('.datepicker').datepicker({ uiLibrary: 'bootstrap', format: 'dd/mm/yyyy' });

    // Datepicker Add LT 1
    $('.datepickerssss').datepicker({ uiLibrary: 'bootstrap', format: 'dd mmmm yyyy' });
    // Datepicker Add LT 2
    $('.datepickerss').datepicker({ uiLibrary: 'bootstrap', format: 'dd mmmm yyyy' });
    // Datepicker Add LT 3
    $('.datepickersss').datepicker({ uiLibrary: 'bootstrap', format: 'dd mmmm yyyy' });

    // Tambah Anggota
    $('#tambah_anggota').click(function() {
      $('#formModal').modal('show');
      $('.modal-title').text('Tambah Anggota');
      $('#action_button').val('Tambah');
      $('#action').val('Add');
      $('#form_result').html();
    });

    $('#sample_form').on('submit', function(event) {
      event.preventDefault();
      var id = $(this).attr('id');
      var action_url = '';

      if ($('#action').val() == 'Add') {
        action_url = "{{ route('tapanuliutara.add') }}";
        text = 'Data anggota berhasil ditambah';
      }

      if ($('#action').val() == 'Edit') {
        action_url = "{{ route('tapanuliutara.update') }}";
        text = 'Data anggota berhasil diupdate';
      }

      $.ajax({
        url: action_url,
        method: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
          var html = '';
          if (data.errors) {
            html = '<div class="alert alert-danger">';
            for (var count = 0; count < data.errors.length; count++) {
              html += '<li><b>'+data.errors[count]+'</b></li>';
            }
            html += '</div>';
          }

          if (data.success) {
            Swal.fire({
              type: 'success',
              icon: 'success',
              title: 'Berhasil',
              text: text
            }).then(function() {
              location.reload();
            });
            $('#formModal').modal('hide');
            // html = '<div class="alert alert-success">'+data.success+'</div>';
            $('#sample_form')[0].reset();
          }
          $('#form_result').html(html);
        }
      });
    });

    // Edit Anggota
    $(document).on('click', '.edit', function() {
      var id = $(this).attr('id');
      $('#form_result').html('');
      $.ajax({
        url: '/tapanuliutara/'+id+'/edit',
        dataType: 'json',
        success: function(data) {
          $('#no_anggota').val(data.result.no_anggota);
          $('#nama').val(data.result.nama);
          $('#jenis_kelamin').val(data.result.jenis_kelamin);
          $('#asal_sekolah').val(data.result.asal_sekolah);
          $('#tempat_lahir').val(data.result.tempat_lahir);
          $('#tanggal_lahir').val(data.result.tanggal_lahir);
          $('#golongan_darah').val(data.result.golongan_darah);
          $('#lead_train_satu').val(data.result.lead_train_satu);
          $('#lead_train_dua').val(data.result.lead_train_dua);
          $('#lead_train_tiga').val(data.result.lead_train_tiga);
          $('#alamat').val(data.result.alamat);
          $('#no_telepon').val(data.result.no_telepon);
          $('#nama_mentor').val(data.result.nama_mentor);
          $('#nama_kelompok').val(data.result.nama_kelompok);
          $('#created_by').val(data.result.created_by);
          $('#hidden_id').val(data.result.id);
          $('.modal-title').val('Edit Informasi Anggota');
          $('#action_button').val('Edit');
          $('#action').val('Edit');
          $('#formModal').modal('show');
        }
      });
    });

    // LT 1
    $(document).on('click', '.ltsatu', function() {
      var id = $(this).attr('id');
      $('#form_results').html('');
      $.ajax({
        url: '/tapanuliutara/'+id+'/lt1',
        dataType: 'json',
        success: function(data) {
          $('#formModals').modal('show');
          $('#action_buttons').val('Update LT 1');
          $('#actions').val('Update');
          $('#lead_train_satus').val(data.data.lead_train_satu);
          $('#hidden_ids').val(data.data.id);
        }
      });
    });

    $('#sample_forms').on('submit', function(event) {
      event.preventDefault();
      var id = $(this).attr('id');

      $.ajax({
        url: "{{ route('tapanuliutara.ltsatuTambah') }}",
        method: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
          var html = '';
          if (data.errors) {
            html = '<div class="alert alert-danger">';
            for (var count = 0; count < data.errors.length; count++) {
                html += '<li><b>'+data.errors[count]+'</b></li>';
            }
            html += '</div>';
          }

          if (data.success) {
            Swal.fire({
              type: 'success',
              icon: 'success',
              title: 'Berhasil',
              text: 'Data LT 1 berhasil ditambah'
            }).then(function() {
              location.reload();
            });
            $('#formModals').modal('hide');
            // html = '<div class="alert alert-success">'+data.success+'</div>';
            $('#sample_forms')[0].reset();
          }
          $('#form_results').html(html);
        }
      });
    });

    // LT 2
    $(document).on('click', '.ltdua', function() {
      var id = $(this).attr('id');
      $('#form_resultss').html('');
      $.ajax({
        url: '/tapanuliutara/'+id+'/lt2',
        dataType: 'json',
        success: function(data) {
          $('#formModalss').modal('show');
          $('#action_buttonss').val('Update LT 2');
          $('#actionss').val('Update');
          $('#lead_train_duass').val(data.data.lead_train_dua);
          $('#hidden_idss').val(data.data.id);
        }
      });
    });

    $('#sample_formss').on('submit', function(event) {
      event.preventDefault();
      var id = $(this).attr('id');

      $.ajax({
        url: "{{ route('tapanuliutara.ltduaTambah') }}",
        method: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
          var html = '';
          if (data.errors) {
            html = '<div class="alert alert-danger">';
            for (var count = 0; count < data.errors.length; count++) {
                html += '<li><b>'+data.errors[count]+'</b></li>';
            }
            html += '</div>';
          }

          if (data.success) {
            Swal.fire({
              type: 'success',
              icon: 'success',
              title: 'Berhasil',
              text: 'Data LT 2 berhasil ditambah'
            }).then(function() {
              location.reload();
            });
            $('#formModalss').modal('hide');
            // html = '<div class="alert alert-success">'+data.success+'</div>';
            $('#sample_formss')[0].reset();
          }
          $('#form_resultss').html(html);
        }
      });
    });

    // LT 3
    $(document).on('click', '.ltiga', function() {
      var id = $(this).attr('id');
      $('#form_resultsss').html('');
      $.ajax({
        url: '/tapanuliutara/'+id+'/lt3',
        dataType: 'json',
        success: function(data) {
          $('#formModalsss').modal('show');
          $('#action_buttonsss').val('Update LT 3');
          $('#actionsss').val('Update');
          $('#lead_train_tigasss').val(data.data.lead_train_tiga);
          $('#hidden_idsss').val(data.data.id);
        }
      });
    });

    $('#sample_formsss').on('submit', function(event) {
      event.preventDefault();
      var id = $(this).attr('id');

      $.ajax({
        url: "{{ route('tapanuliutara.lttigaTambah') }}",
        method: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
          var html = '';
          if (data.errors) {
            html = '<div class="alert alert-danger">';
            for (var count = 0; count < data.errors.length; count++) {
                html += '<li><b>'+data.errors[count]+'</b></li>';
            }
            html += '</div>';
          }

          if (data.success) {
            Swal.fire({
              type: 'success',
              icon: 'success',
              title: 'Berhasil',
              text: 'Data LT 3 berhasil ditambah'
            }).then(function() {
              location.reload();
            });
            $('#formModalsss').modal('hide');
            // html = '<div class="alert alert-success">'+data.success+'</div>';
            $('#sample_formsss')[0].reset();
          }
          $('#form_resultsss').html(html);
        }
      });
    });
  });
  
  function deleteConfirmation(id) {
    Swal.fire({
      icon: 'warning',
      title: "Delete?",
      text: "Please ensure and then confirm!",
      type: "warning",
      showCancelButton: !0,
      confirmButtonText: "Yes, Delete!",
      cancelButtonText: "No, Cancel!",
      reverseButtons: !0
    }).then(function (event) {
      if (event.value === true) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
          type: 'POST',
          url: '/tapanuliutara/destroy/'+id,
          data: { _token: CSRF_TOKEN },
          dataType: 'JSON',
          success: function (results) {
            if (results.success === true) {
              Swal.fire({
                toast: false,
                icon: 'success',
                title: "Success",
                text: "Data Deleted",
                showConfirmButton: true,
                // timer: 1500
              }).then(function() {
                location.reload();
              });
              $('#sample_form')[0].reset();
            } else {
              Swal.fire("Error!", results.message, "error");
            }
          }
        });
      } else {
        e.dismiss;
      }
    }, function (dismiss) {
      return false;
    });
  }
  
  // Alert LT 1, LT 2, LT 3
  function alertsatu() {
    Swal.fire({
      icon: 'warning',
      title: 'Ooops!',
      text: 'Anda harus menyelesaikan Lead Training Satu Terlebih Dahulu'
    });
  }

  function alertdua() {
    Swal.fire({
      icon: 'warning',
      title: 'Ooops!',
      text: 'Anda harus menyelesaikan Lead Training Dua Terlebih Dahulu'
    });
  }

  // Input nomor saja
  function isNumberKey(evt)
  {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

      return true;
  }
</script>
<!-- End Script -->
@endsection