<!-- Modals LT 3 -->
<div id="formModalsss" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Anggota</h4>
            </div>
            <div class="modal-body">
                <span id="form_resultsss"></span>
                <form method="post" id="sample_formsss" class="form-horizontal">
                @csrf
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label>Tanggal LT 3:</label>
                          <input type="text" name="lead_train_tiga" id="lead_train_tigasss" class="datepickersss form-control" placeholder="Pilih Tanggal" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col">
                        <div class="form-group" align="center">
                          <input type="hidden" name="actionsss" id="actionsss" value="Add">
                          <input type="hidden" name="hidden_idsss" id="hidden_idsss">
                          <input type="submit" name="action_buttonsss" value="Add" id="action_buttonsss" class="btn btn-primary btn-block">
                        </div>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals LT 3 -->
