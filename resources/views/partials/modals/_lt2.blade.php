<!-- Modals LT 2 -->
<div id="formModalss" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update LT 2</h4>
            </div>
            <div class="modal-body">
                <span id="form_resultss"></span>
                <form method="post" id="sample_formss" class="form-horizontal">
                @csrf
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label>Tanggal LT 2:</label>
                          <input type="text" name="lead_train_dua" id="lead_train_duass" class="datepickerss form-control" placeholder="Pilih Tanggal" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col">
                        <div class="form-group" align="center">
                          <input type="hidden" name="actionss" id="actionss" value="Add">
                          <input type="hidden" name="hidden_idss" id="hidden_idss">
                          <input type="submit" name="action_buttonss" value="Add" id="action_buttonss" class="btn btn-primary btn-block">
                        </div>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals LT 2 -->
