<!-- Modals LT 1 -->
<div id="formPass" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ganti Password</h4>
            </div>
            <div class="modal-body">
                <span id="form_res"></span>
                <form method="post" id="sample" class="form-horizontal">
                @csrf
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label for="password">Password baru:</label>
                          <input type="password" name="password" id="password" class="form-control">
                          <span toggle="#password" class="fa fa-lg fa-eye field-icon toggle-password"></span>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col">
                        <div class="form-group" align="center">
                          <input type="hidden" name="actionn" id="actionn" value="Add">
                          <input type="hidden" name="id_hidden" id="id_hidden">
                          <input type="submit" name="actionbutton" value="Update" id="actionbutton" class="btn btn-primary btn-block">
                        </div>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals LT 1 -->
