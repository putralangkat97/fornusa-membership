<!-- Modals LT 1 -->
<div id="formModals" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update LT 1</h4>
            </div>
            <div class="modal-body">
                <span id="form_results"></span>
                <form method="post" id="sample_forms" class="form-horizontal">
                @csrf
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label>Tanggal LT 1:</label>
                          <input type="text" name="lead_train_satu" id="lead_train_satus" class="datepickerssss form-control" placeholder="Pilih Tanggal" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col">
                        <div class="form-group" align="center">
                          <input type="hidden" name="actions" id="actions" value="Add">
                          <input type="hidden" name="hidden_ids" id="hidden_ids">
                          <input type="submit" name="action_buttons" value="Add" id="action_buttons" class="btn btn-primary btn-block">
                        </div>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals LT 1 -->
