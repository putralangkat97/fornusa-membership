<!-- Modals Tambah Anggota -->

<div id="formModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Anggota</h4>
      </div>

      <div class="modal-body">

        <span id="form_result"></span>
        <form method="post" id="sample_form" class="form-horizontal">
        @csrf

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>No. Anggota</label>
                <input type="text" name="no_anggota" class="form-control" readonly value="{{ $number }}" id="no_anggota">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Nama:</label>
                <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" autocomplete="off" id="nama">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Asal Sekolah:</label>
                <input type="text" name="asal_sekolah" class="form-control" placeholder="Asal Sekolah" autocomplete="off" id="asal_sekolah">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Jenis Kelamin:</label>
                <select name="jenis_kelamin" class="form-control" id="jenis_kelamin">
                  <option value="">Jenis Kelamin</option>
                  <option value="L">Laki-Laki</option>
                  <option value="P">Perempuan</option>
                </select>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Golongan Darah:</label>
                <select name="golongan_darah" class="form-control" id="golongan_darah">
                  <option value="">Golongan Darah</option>
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="O">O</option>
                  <option value="AB">AB</option>
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Tempat Lahir:</label>
                <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" autocomplete="off" id="tempat_lahir">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Tgl. Lahir:</label>
                <input type="text" name="tanggal_lahir" class="datepicker form-control" placeholder="Tanggal Lahir" autocomplete="off" id="tanggal_lahir">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>No. Telepon:</label>
                <input type="text" name="no_telepon" class="form-control" placeholder="Nomor Handphone" autocomplete="off" id="no_telepon" onkeypress="return isNumberKey(event)">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Alamat:</label>
                <textarea name="alamat" cols="10" rows="2" class="form-control" placeholder="Alamat" autocomplete="off" id="alamat"></textarea>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Nama Mentor:</label>
                <select name="nama_mentor" id="nama_mentor" class="form-control">
                <option selected>Pilih Mentor</option>
                  @foreach($data as $row)
                  <option value="{{ $row->name }}">{{ $row->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label>Nama Kelompok: </label>
                <input type="text" name="nama_kelompok" id="nama_kelompok" class="form-control" autocomplete="off" placeholder="Nama Kelompok">
              </div>
            </div>
            <input type="hidden" name="lead_train_satu" id="lead_train_satu" class="datepickers form-group" placeholder="Pilih Tanggal" autocomplete="off">
            <input type="hidden" name="lead_train_dua" id="lead_train_dua" class="datepickers form-group" placeholder="Pilih Tanggal" autocomplete="off">
            <input type="hidden" name="lead_train_tiga" id="lead_train_tiga" class="datepickers form-group" placeholder="Pilih Tanggal" autocomplete="off">
          </div>

          <br>
          
          <div class="row">
            <div class="col">
              <div class="form-group" align="center">
                <input type="hidden" name="action" id="action" value="Add">
                <input type="hidden" name="hidden_id" id="hidden_id">
                <input type="submit" name="action_button" value="Add" id="action_button" class="btn btn-success btn-block">
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

<!-- End Modals Tambah Anggota -->
