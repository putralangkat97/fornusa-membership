<!--- Sidemenu -->
<div id="sidebar-menu">

<ul class="metismenu" id="side-menu">

    <li class="menu-title">Navigation</li>

    @hasrole('author')
    <li>
        <a href="{{ URL('/admin') }}">
            <i class="fe-airplay"></i>
            <!-- <span class="badge badge-success badge-pill float-right">2</span> -->
            <span> Dashboard </span>
            <!-- <span class="menu-arrow"></span> -->
        </a>
        <!-- <ul class="nav-second-level" aria-expanded="false">
            <li><a href="index-2.html">Dashboard 1</a></li>
            <li> <a href="dashboard-2.html">Dashboard 2</a></li>
        </ul> -->
    </li>

    <li>
        <a href="{{ route('author.user') }}"><i class="fe-user"></i> <span>Data Admin</span></a>
    </li>
    <li>
        <a href="{{ route('author.mentor') }}"><i class="fe-clipboard"></i> <span>Data Mentor</span></a>
    </li>
    @endhasrole



    @hasrole('asahan')
    <li>
        <a href="{{ route('asahan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('asahan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    <li><a href="{{ route('asahan.setting') }}"><i class="fe-settings"></i> <span>Setting</span></a></li>
    @endhasrole



    @hasrole('batubara')
    <li>
        <a href="{{ route('batubara.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('batubara.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('dairi')
    <li>
        <a href="{{ route('dairi.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('dairi.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    <li>
        <a href="{{ route('dairi.setting') }}"><i class="fe-settings"></i> <span>Setting</span></a>
    </li>
    @endhasrole



    @hasrole('deliserdang')
    <li>
        <a href="{{ route('deliserdang.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('deliserdang.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('humbanghasundutan')
    <li>
        <a href="{{ route('humbanghasundutan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('humbanghasundutan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('karo')
    <li>
        <a href="{{ route('karo.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('karo.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('labuhanbatu')
    <li>
        <a href="{{ route('labuhanbatu.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('labuhanbatu.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('labuhanbatuselatan')
    <li>
        <a href="{{ route('labuhanbatuselatan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('labuhanbatuselatan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('labuhanbatuutara')
    <li>
        <a href="{{ route('labuhanbatuutara.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('labuhanbatuutara.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('langkat')
    <li>
        <a href="{{ route('langkat.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('langkat.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('mandailingnatal')
    <li>
        <a href="{{ route('mandailingnatal.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('mandailingnatal.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('nias')
    <li>
        <a href="{{ route('nias.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('nias.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('niasbarat')
    <li>
        <a href="{{ route('niasbarat.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('niasbarat.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('niasselatan')
    <li>
        <a href="{{ route('niasselatan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('niasselatan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('niasutara')
    <li>
        <a href="{{ route('niasutara.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('niasutara.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('padanglawas')
    <li>
        <a href="{{ route('padanglawas.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('padanglawas.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('padanglawasutara')
    <li>
        <a href="{{ route('padanglawasutara.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('padanglawasutara.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('pakpakbharat')
    <li>
        <a href="{{ route('pakpakbharat.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('pakpakbharat.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('samosir')
    <li>
        <a href="{{ route('samosir.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('samosir.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('serdangbedagai')
    <li>
        <a href="{{ route('serdangbedagai.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('serdangbedagai.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('simalungun')
    <li>
        <a href="{{ route('simalungun.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('simalungun.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('tapanuliselatan')
    <li>
        <a href="{{ route('tapanuliselatan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('tapanuliselatan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('tapanulitengah')
    <li>
        <a href="{{ route('tapanulitengah.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('tapanulitengah.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('tapanuliutara')
    <li>
        <a href="{{ route('tapanuliutara.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('tapanuliutara.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('tobasamosir')
    <li>
        <a href="{{ route('tobasamosir.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('tobasamosir.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('binjai')
    <li>
        <a href="{{ route('binjai.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('binjai.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('gunungsitoli')
    <li>
        <a href="{{ route('gunungsitoli.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('gunungsitoli.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('medan')
    <li>
        <a href="{{ route('medan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('medan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('padangsidempuan')
    <li>
        <a href="{{ route('padangsidempuan.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('padangsidempuan.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('pematangsiantar')
    <li>
        <a href="{{ route('pematangsiantar.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('pematangsiantar.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('sibolga')
    <li>
        <a href="{{ route('sibolga.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('sibolga.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('tanjungbalai')
    <li>
        <a href="{{ route('tanjungbalai.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('tanjungbalai.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole



    @hasrole('tebingtinggi')
    <li>
        <a href="{{ route('tebingtinggi.dashboard') }}"><i class="fe-airplay"></i> <span>Dashboard</span></a>
    </li>
    <li>
        <a href="{{ route('tebingtinggi.all') }}"><i class="fe-users"></i> <span>Data Member</span></a>
    </li>
    @endhasrole

    <!-- <li>
        <a href="#"><i class="fe-list"></i> <span>Role</span></a>
    </li> -->



    <!-- <li>
        <a href="javascript: void(0);">
            <i class="fe-folder-plus"></i>
            <span> Multi Level </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level nav" aria-expanded="false">
            <li>
                <a href="javascript: void(0);">Level 1.1</a>
            </li>
            <li>
                <a href="javascript: void(0);" aria-expanded="false">Level 1.2
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-third-level nav" aria-expanded="false">
                    <li>
                        <a href="javascript: void(0);">Level 2.1</a>
                    </li>
                    <li>
                        <a href="javascript: void(0);">Level 2.2</a>
                    </li>
                </ul>
            </li>
        </ul>
    </li> -->

</ul>

</div>
<!-- End Sidebar -->
