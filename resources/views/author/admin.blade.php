@extends('layouts.master')

@section('content')
<div class="card-box table-responsive">
    <h4 class="header-title"></h4>
    <div class="sub-header">
        <button type="button" name="tambah_admin" id="tambah_admin" class="btn btn-primary btn-sm sub-header text-white"><i class="fa fa-plus"></i></button>
    </div>

    <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Nama Lengkap</th>
                <th>Username</th>
                <th>E-Mail</th>
                <th>Daerah</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($admin as $key => $a)
            <tr>
                <td class="text-center">{{ ++$key }}</td>
                <td>{{ $a->name }}</td>
                <td>{{ $a->username }}</td>
                <td>{{ $a->email }}</td>
                <td>{{ implode(', ', $a->roles()->get()->pluck('nama_kota')->toArray()) }}</td>
                <td class="text-center">
                    <a href="#" class="edit" id="{{ $a->id }}"><i class="fa fa-edit text-info"></i></a>
                    &nbsp;
                    <a href="#" class="delete" onclick="deleteConfirmation({{ $a->id }})"><i class="fa fa-trash text-danger"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<!-- Modals -->
<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Baru</h4>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Nama:</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Nama Lengkap" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Username:</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Username Tidak Boleh Menggunakan Spasi" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">E-Mail:</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="E-Mail" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Password:</label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password Min. Length 6" autocomplete="off" minlength="6">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Role:</label>
                                <select name="role" id="role" class="form-control" autocomplete="off">
                                    <option value="">Select Role</option>
                                    @foreach($roles as $role)
                                    <option value="{{ $role->name }}">{{ $role->nama_kota }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col">
                            <div class="form-group" align="center">
                                <input type="hidden" name="action" id="action" value="Add">
                                <input type="hidden" name="hidden_id" id="hidden_id">
                                <input type="submit" name="action_button" value="Add" id="action_button" class="btn btn-primary btn-block">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Script -->
<script>
    $(document).ready(function() {
        $('#datatable').DataTable();

        $('#tambah_admin').click(function() {
            $('.modal-title').text('Tambah Admin');
            $('#action_button').val('Tambah');
            $('#action').val('Add');
            $('#form_result').html('');
            $('#formModal').modal('show');
        });

        $('#sample_form').on('submit', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');
            var action_url = '';

            if ($('#action').val() == 'Add') {
                action_url = "{{ route('author.author.store') }}";
            }

            $.ajax({
                url: action_url,
                method: 'POST',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(data)
                {
                    var html = '';
                    if (data.errors) {
                        html = '<div class="alert alert-danger">';
                        for (var count = 0; count < data.errors.length; count++) {
                            html += '<li><b>'+data.errors[count]+'</b></li>';
                        }
                        html += '</div>';
                    }

                    if (data.success) {
                        Swal.fire({
                            type: 'success',
                            icon: 'success',
                            title: 'Berhasil',
                            text: 'Berhasil ditambahkan'
                        }).then(function() {
                            location.reload();
                        });
                        $('#formModal').modal('hide');
                        // html = '<div class="alert alert-success">'+data.success+'</div>';
                        $('#sample_form')[0].reset();
                    }
                    $('#form_result').html(html);
                }
            });
        });

        $(document).on('click', '.edit', function() {
            var id = $(this).attr('id');
            $.ajax({
                url: '/author/'+id+'/edit',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                }
            });
        })

        $("input#username").on({
            keydown: function(e) {
                if (e.which === 32)
                return false;
            },
            change: function() {
                this.value = this.value.replace(/\s/g, "");
            }
        });
    });
</script>
@endsection