@extends('layouts.master')

@section('content')
<div class="card-box table-responsive">
    <h4 class="header-title"></h4>
    <div class="sub-header">
        <button type="button" name="tambah_mentor" id="tambah_mentor" class="btn btn-primary btn-sm sub-header text-white"><i class="fa fa-plus"></i></button>
    </div>

    <table id="datatable" class="table table-bordered  dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Nama Lengkap</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($mentor as $key => $m)
            <tr>
                <td class="text-center">{{ ++$key }}</td>
                <td>{{ $m->name }}</td>
                <td class="text-center">
                  <a href="#" class="edit" id="{{ $m->id }}"><i class="fa fa-edit text-info"></i></a>
                  &nbsp;
                  <a href="#" class="delete" onclick="deleteConfirmation({{ $m->id }})"><i class="fa fa-trash text-danger"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<!-- Modals -->
<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Baru</h4>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Nama:</label>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Nama Lengkap" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col">
                            <div class="form-group" align="center">
                                <input type="hidden" name="action" id="action" value="Add">
                                <input type="hidden" name="hidden_id" id="hidden_id">
                                <input type="submit" name="action_button" value="Add" id="action_button" class="btn btn-primary btn-block">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Script -->
<script>
  $(document).ready(function() {
      $('#datatable').DataTable();
      $('#tambah_mentor').click(function() {
         $('.modal-title').text('Tambah Mentor');
         $('#action_button').val('Tambah');
         $('#action').val('Add');
         $('#form_result').html('');
         $('#formModal').modal('show');
      });

      $('#sample_form').on('submit', function(event) {
         event.preventDefault();
         var id = $(this).attr('id');
         var action = '';

         if ($('#action').val() == 'Add') {
            action = "{{ route('author.addMentor') }}";
            text = "Mentor berhasil ditambahkan";
         }

         if ($('#action').val() == 'Edit') {
            action = "{{ route('author.updateMentor') }}";
            text = "Data Mentor berhasil diupdate";
         }

         $.ajax({
            url: action,
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
               var html = '';
               if (data.errors) {
                  html = '<div class="alert alert-danger">';
                  for (var count = 0; count < data.errors.length; count++) {
                        html += '<li><b>'+data.errors[count]+'</b></li>';
                  }
                  html += '</div>';
               }

               if(data.success) {
                  Swal.fire({
                     type: 'success',
                     icon: 'success',
                     title: 'Berhasil',
                     text: 'Berhasil ditambahkan'
                  }).then(function() {
                     location.reload();
                  });
                  $('#formModal').modal('hide');
                  // html = '<div class="alert alert-success">'+data.success+'</div>';
                  $('#sample_form')[0].reset();
               }
               $('#form_result').html(html);
            }
         });
      });
   });

   $(document).on('click', '.edit', function() {
      var id = $(this).attr('id');
      $('#form_result').html('');
      $.ajax({
         url: '/mentor/'+id+'/edit',
         dataType: 'json',
         success: function(data) {
            $('#name').val(data.result.name);
            $('#hidden_id').val(data.result.id);
            $('#formModal').modal('show');
            $('.modal-title').text('Edit Mentor');
            $('#action').val('Edit');
            $('#action_button').val('Edit');
         }
      });
   });

   function deleteConfirmation(id) {
    Swal.fire({
        icon: "warning",
        title: "Hapus?",
        text: "Apa anda yakin?",
        type: "warning",
        showCancelButton: !0,
        cinfirmButtonText: "Ya, hapus!",
        cancelButtonText: "Batal",
        reverseButtons: !0
    }).then(function(event) {
      if(event.value === true)
      {
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

         $.ajax({
          type: 'POST',
          url: '/mentor/delete/'+id,
          data: { _token: CSRF_TOKEN },
          dataType: 'JSON',
          success: function(results){
            if(results.success === true)
            {
              Swal.fire({
                icon: 'success',
                type: 'success',
                title: 'Berhasil',
                text: 'Data berhasil dihapus'
              }).then(function(){
                location.reload();
              });
            } else {
              Swal.fire("Error!", results.message, "error");
            }
          }
        });
      } else {
        e.dismiss;
      }
    }, function(dismiss){
      return false;
    });
  }
</script>
@endsection