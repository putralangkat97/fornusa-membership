@extends('layouts.master')

@section('title')
Dashboard | {{ Auth::user()->username }}
@endsection

@section('content')
<h1>Dashboard</h1>

<div class="row">
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                  <i class="fe-users avatar-title font-30 text-white"></i>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total Anggota</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $total }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-danger widget-two-icon align-self-center">
                  <span class="avatar-title font-30 text-white">LT1</span>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total LT 1</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $lt1 - $lt2 }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-info widget-two-icon align-self-center">
                  <span class="avatar-title font-30 text-white">LT2</span>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total LT 2</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $lt2 - $lt3 }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
  <div class="col-xl-3 col-sm-6">
  <a href="#">
      <div class="card-box widget-box-two widget-two-custom">
          <div class="media">
              <div class="avatar-lg rounded-circle bg-success widget-two-icon align-self-center">
                  <span class="avatar-title font-30 text-white">LT3</span>
              </div>

              <div class="wigdet-two-content media-body">
                  <p class="m-0 text-uppercase font-weight-medium text-primary" title="Statistics">Total LT 3</p>
                  <h3 class="font-weight-medium my-2"><span data-plugin="counterup">{{ $lt3 }}</span></h3>
              </div>
          </div>
      </div>
  </a>
  </div>
</div>
<div class="card-box">
<table class="table table-bordered" id="datatable">
    <thead>
        <tr>
            <th>#</th>
            <th>Wilayah</th>
            <th>Total Anggota</th>
            <th>LT 1</th>
            <th>LT 2</th>
            <th>LT 3</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Asahan</td>
            <td>{{ $asahan }}</td>
            <td>{{ $asahanlt1 }}</td>
            <td>{{ $asahanlt2 }}</td>
            <td>{{ $asahanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Batu Bara</td>
            <td>{{ $batubara }}</td>
            <td>{{ $batubaralt1 }}</td>
            <td>{{ $batubaralt2 }}</td>
            <td>{{ $batubaralt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Dairi</td>
            <td>{{ $dairi }}</td>
            <td>{{ $dairilt1 }}</td>
            <td>{{ $dairilt2 }}</td>
            <td>{{ $dairilt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Deli Serdang</td>
            <td>{{ $deliserdang }}</td>
            <td>{{ $deliserdanglt1 }}</td>
            <td>{{ $deliserdanglt2 }}</td>
            <td>{{ $deliserdanglt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Humbang Hasundutan</td>
            <td>{{ $humbanghasundutan }}</td>
            <td>{{ $humbanghasundutanlt1 }}</td>
            <td>{{ $humbanghasundutanlt2 }}</td>
            <td>{{ $humbanghasundutanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Karo</td>
            <td>{{ $karo }}</td>
            <td>{{ $karolt1 }}</td>
            <td>{{ $karolt2 }}</td>
            <td>{{ $karolt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Labuhan Batu</td>
            <td>{{ $labuhanbatu }}</td>
            <td>{{ $labuhanbatult1 }}</td>
            <td>{{ $labuhanbatult2 }}</td>
            <td>{{ $labuhanbatult3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Labuhan Batu Selatan</td>
            <td>{{ $labuhanbatuselatan }}</td>
            <td>{{ $labuhanbatuselatanlt1 }}</td>
            <td>{{ $labuhanbatuselatanlt2 }}</td>
            <td>{{ $labuhanbatuselatanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>9</td>
            <td>Labuhan Batu Utara</td>
            <td>{{ $labuhanbatuutara }}</td>
            <td>{{ $labuhanbatuutaralt1 }}</td>
            <td>{{ $labuhanbatuutaralt2 }}</td>
            <td>{{ $labuhanbatuutaralt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Langkat</td>
            <td>{{ $langkat }}</td>
            <td>{{ $langkatlt1 }}</td>
            <td>{{ $langkatlt2 }}</td>
            <td>{{ $langkatlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>11</td>
            <td>Mandailing Natal</td>
            <td>{{ $mandailingnatal }}</td>
            <td>{{ $mandailingnatallt1 }}</td>
            <td>{{ $mandailingnatallt2 }}</td>
            <td>{{ $mandailingnatallt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>12</td>
            <td>Nias</td>
            <td>{{ $nias }}</td>
            <td>{{ $niaslt1 }}</td>
            <td>{{ $niaslt2 }}</td>
            <td>{{ $niaslt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>13</td>
            <td>Nias Barat</td>
            <td>{{ $niasbarat }}</td>
            <td>{{ $niasbaratlt1 }}</td>
            <td>{{ $niasbaratlt2 }}</td>
            <td>{{ $niasbaratlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>14</td>
            <td>Nias Selatan</td>
            <td>{{ $niasselatan }}</td>
            <td>{{ $niasselatanlt1 }}</td>
            <td>{{ $niasselatanlt2 }}</td>
            <td>{{ $niasselatanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>15</td>
            <td>Nias Utara</td>
            <td>{{ $niasutara }}</td>
            <td>{{ $niasutaralt1 }}</td>
            <td>{{ $niasutaralt2 }}</td>
            <td>{{ $niasutaralt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>16</td>
            <td>Padang Lawas</td>
            <td>{{ $padanglawas }}</td>
            <td>{{ $padanglawaslt1 }}</td>
            <td>{{ $padanglawaslt2 }}</td>
            <td>{{ $padanglawaslt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>17</td>
            <td>Padang Lawas Utara</td>
            <td>{{ $padanglawasutara }}</td>
            <td>{{ $padanglawasutaralt1 }}</td>
            <td>{{ $padanglawasutaralt2 }}</td>
            <td>{{ $padanglawasutaralt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>18</td>
            <td>Pakpak Bharat</td>
            <td>{{ $pakpakbharat }}</td>
            <td>{{ $pakpakbharatlt1 }}</td>
            <td>{{ $pakpakbharatlt2 }}</td>
            <td>{{ $pakpakbharatlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>19</td>
            <td>Samosir</td>
            <td>{{ $samosir }}</td>
            <td>{{ $samosirlt1 }}</td>
            <td>{{ $samosirlt2 }}</td>
            <td>{{ $samosirlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>20</td>
            <td>Serdang Bedagai</td>
            <td>{{ $serdangbedagai }}</td>
            <td>{{ $serdangbedagailt1 }}</td>
            <td>{{ $serdangbedagailt2 }}</td>
            <td>{{ $serdangbedagailt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>21</td>
            <td>Simalungun</td>
            <td>{{ $simalungun }}</td>
            <td>{{ $simalungunlt1 }}</td>
            <td>{{ $simalungunlt2 }}</td>
            <td>{{ $simalungunlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>22</td>
            <td>Tapanuli Selatan</td>
            <td>{{ $tapanuliselatan }}</td>
            <td>{{ $tapanuliselatanlt1 }}</td>
            <td>{{ $tapanuliselatanlt2 }}</td>
            <td>{{ $tapanuliselatanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>23</td>
            <td>Tapanuli Tengah</td>
            <td>{{ $tapanulitengah }}</td>
            <td>{{ $tapanulitengahlt1 }}</td>
            <td>{{ $tapanulitengahlt2 }}</td>
            <td>{{ $tapanulitengahlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>24</td>
            <td>Tapanuli Utara</td>
            <td>{{ $tapanuliutara }}</td>
            <td>{{ $tapanuliutaralt1 }}</td>
            <td>{{ $tapanuliutaralt2 }}</td>
            <td>{{ $tapanuliutaralt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>25</td>
            <td>Toba Samosir</td>
            <td>{{ $tobasamosir }}</td>
            <td>{{ $tobasamosirlt1 }}</td>
            <td>{{ $tobasamosirlt2 }}</td>
            <td>{{ $tobasamosirlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>26</td>
            <td>Binjai</td>
            <td>{{ $binjai }}</td>
            <td>{{ $binjailt1 }}</td>
            <td>{{ $binjailt2 }}</td>
            <td>{{ $binjailt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>27</td>
            <td>Gunungsitoli</td>
            <td>{{ $gunungsitoli }}</td>
            <td>{{ $gunungsitolilt1 }}</td>
            <td>{{ $gunungsitolilt2 }}</td>
            <td>{{ $gunungsitolilt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>28</td>
            <td>Medan</td>
            <td>{{ $medan }}</td>
            <td>{{ $medanlt1 }}</td>
            <td>{{ $medanlt2 }}</td>
            <td>{{ $medanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>29</td>
            <td>Padangsidempuan</td>
            <td>{{ $padangsidempuan }}</td>
            <td>{{ $padangsidempuanlt1 }}</td>
            <td>{{ $padangsidempuanlt2 }}</td>
            <td>{{ $padangsidempuanlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>30</td>
            <td>Pematangsiantar</td>
            <td>{{ $pematangsiantar }}</td>
            <td>{{ $pematangsiantarlt1 }}</td>
            <td>{{ $pematangsiantarlt2 }}</td>
            <td>{{ $pematangsiantarlt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>31</td>
            <td>Sibolga</td>
            <td>{{ $sibolga }}</td>
            <td>{{ $sibolgalt1 }}</td>
            <td>{{ $sibolgalt2 }}</td>
            <td>{{ $sibolgalt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>32</td>
            <td>Tanjungbalai</td>
            <td>{{ $tanjungbalai }}</td>
            <td>{{ $tanjungbalailt1 }}</td>
            <td>{{ $tanjungbalailt2 }}</td>
            <td>{{ $tanjungbalailt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
        <tr>
            <td>33</td>
            <td>Tebing Tinggi</td>
            <td>{{ $tebingtinggi }}</td>
            <td>{{ $tebingtinggilt1 }}</td>
            <td>{{ $tebingtinggilt2 }}</td>
            <td>{{ $tebingtinggilt3 }}</td>
            <td><a href="#" class="btn btn-sm btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>
        </tr>
    </tbody>
</table>
</div>
<!-- end row -->
@endsection