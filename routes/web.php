<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

////////////
// AUTHOR //
////////////
Route::namespace('Author')
    ->name('author.')
    ->middleware(['auth', 'auth.author'])
    ->group(function() {
        Route::get('/admin', 'AuthorController@index')
            ->name('index');
        Route::post('/admin/store', 'AuthorController@store')
            ->name('author.store');
        Route::get('/admin/admin_list', 'AuthorController@admin')
            ->name('user');
        Route::get('/admin/{id}/edit', 'AuthorController@edit');
        Route::get('/admin/mentor', 'AuthorController@mentor')
            ->name('mentor');
        Route::post('/mentor/add', 'AuthorController@addMentor')
            ->name('addMentor');
        Route::get('/mentor/{id}/edit', 'AuthorController@editMentor');
        Route::post('/mentor/update', 'AuthorController@updateMentor')
            ->name('updateMentor');
        Route::post('/mentor/delete/{id}', 'AuthorController@deleteMentor');
});


////////////////
// 01 ASAHAN //
///////////////
Route::namespace('AdminAsahan')
    ->name('asahan.')
    ->middleware(['auth', 'auth.asahan'])
    ->group(function() {
        Route::get('/asahan', 'AdminController@index')
            ->name('dashboard');
        Route::get('/asahan/all', 'AdminController@all')
            ->name('all');
        Route::get('/asahan/{id}/edit', 'AdminController@edit');
        Route::post('/asahan/add', 'AdminController@add')
            ->name('add');
        Route::post('/asahan/update', 'AdminController@update')
            ->name('update');
        Route::post('/asahan/destroy/{id}', 'AdminController@destroy');
        Route::post('/asahan/cancel/{id}/lt1', 'AdminController@cancelLt1');
        Route::post('/asahan/cancel/{id}/lt2', 'AdminController@cancelLt2');
        Route::post('/asahan/cancel/{id}/lt3', 'AdminController@cancelLt3');

        // LT 1, LT 2, LT 3
        Route::get('/asahan/lt1', 'AdminController@lt1')
            ->name('ltsatu');
        Route::get('/asahan/lt2', 'AdminController@lt2')
            ->name('ltdua');
        Route::get('/asahan/lt3', 'AdminController@lt3')
            ->name('lttiga');
        Route::get('/asahan/{id}/lt1', 'AdminController@ltsatu');
        Route::get('/asahan/{id}/lt2', 'AdminController@ltdua');
        Route::get('/asahan/{id}/lt3', 'AdminController@lttiga');
        Route::post('/asahan/tambah/lt1', 'AdminController@ltsatuTambah')
            ->name('ltsatuTambah');
        Route::post('/asahan/tambah/lt2', 'AdminController@ltduaTambah')
            ->name('ltduaTambah');
        Route::post('/asahan/tambah/lt3', 'AdminController@lttigaTambah')
            ->name('lttigaTambah');

        // Export into Excel, PDF
        Route::get('/asahan/excel', 'AdminController@excel')
            ->name('excel');

        // PDF Generate
        Route::get('/asahan/pdf', 'AdminController@pdfgenerate')
            ->name('pdf');
        Route::get('/asahan/print', 'AdminController@pdfprint')
            ->name('print');
        Route::get('/asahan/card/{id}', 'AdminController@card')
            ->name('card');
        Route::get('/asahan/setting', 'AdminController@setting')
            ->name('setting');
        Route::post('/asahan/setting/update', 'AdminController@changePass');
});


//////////////////
// 02 BATU BARA //
//////////////////
Route::namespace('AdminBatubara')
    ->name('batubara.')
    ->middleware(['auth', 'auth.batubara'])
    ->group(function() {
        Route::get('/batubara', 'AdminController@index')
            ->name('dashboard');
        Route::get('/batubara/all', 'AdminController@all')
            ->name('all');
        Route::get('/batubara/{id}/edit', 'AdminController@edit');
        Route::post('/batubara/add', 'AdminController@add')
            ->name('add');
        Route::post('/batubara/update', 'AdminController@update')
            ->name('update');
        Route::post('/batubara/destroy/{id}', 'AdminController@destroy');
        Route::post('/batubara/cancel/{id}/lt1', 'AdminController@cancelLt1');
        Route::post('/batubara/cancel/{id}/lt2', 'AdminController@cancelLt2');
        Route::post('/batubara/cancel/{id}/lt3', 'AdminController@cancelLt3');

        // LT 1, LT 2, LT 3
        Route::get('/batubara/lt1', 'AdminController@lt1')
            ->name('ltsatu');
        Route::get('/batubara/lt2', 'AdminController@lt2')
            ->name('ltdua');
        Route::get('/batubara/lt3', 'AdminController@lt3')
            ->name('lttiga');
        Route::get('/batubara/{id}/lt1', 'AdminController@ltsatu');
        Route::get('/batubara/{id}/lt2', 'AdminController@ltdua');
        Route::get('/batubara/{id}/lt3', 'AdminController@lttiga');
        Route::post('/batubara/tambah/lt1', 'AdminController@ltsatuTambah')
            ->name('ltsatuTambah');
        Route::post('/batubara/tambah/lt2', 'AdminController@ltduaTambah')
            ->name('ltduaTambah');
        Route::post('/batubara/tambah/lt3', 'AdminController@lttigaTambah')
            ->name('lttigaTambah');

        // Export into Excel, PDF
        Route::get('/batubara/excel', 'AdminController@excel')
            ->name('excel');

        // PDF Generate
        Route::get('/batubara/pdf', 'AdminController@pdfgenerate')
            ->name('pdf');
        Route::get('/batubara/print', 'AdminController@pdfprint')
            ->name('print');
        Route::get('/batubara/card/{id}', 'AdminController@card')
            ->name('card');
        Route::get('/batubara/setting', 'AdminController@setting')
            ->name('setting');
        Route::post('/batubara/setting/update', 'AdminController@changePass');
});


//////////////
// 03 DAIRI //
//////////////
Route::namespace('AdminDairi')
    ->name('dairi.')
    ->middleware(['auth', 'auth.dairi'])
    ->group(function() {
        Route::get('/dairi', 'AdminController@index')
            ->name('dashboard');
        Route::get('/dairi/all', 'AdminController@all')
            ->name('all');
        Route::get('/dairi/{id}/edit', 'AdminController@edit');
        Route::post('/dairi/add', 'AdminController@add')
            ->name('add');
        Route::post('/dairi/update', 'AdminController@update')
            ->name('update');
        Route::post('/dairi/destroy/{id}', 'AdminController@destroy');
        Route::post('/dairi/cancel/{id}/lt1', 'AdminController@cancelLt1');
        Route::post('/dairi/cancel/{id}/lt2', 'AdminController@cancelLt2');
        Route::post('/dairi/cancel/{id}/lt3', 'AdminController@cancelLt3');

        // LT 1, LT 2, LT 3
        Route::get('/dairi/lt1', 'AdminController@lt1')
            ->name('ltsatu');
        Route::get('/dairi/lt2', 'AdminController@lt2')
            ->name('ltdua');
        Route::get('/dairi/lt3', 'AdminController@lt3')
            ->name('lttiga');
        Route::get('/dairi/{id}/lt1', 'AdminController@ltsatu');
        Route::get('/dairi/{id}/lt2', 'AdminController@ltdua');
        Route::get('/dairi/{id}/lt3', 'AdminController@lttiga');
        Route::post('/dairi/tambah/lt1', 'AdminController@ltsatuTambah')
            ->name('ltsatuTambah');
        Route::post('/dairi/tambah/lt2', 'AdminController@ltduaTambah')
            ->name('ltduaTambah');
        Route::post('/dairi/tambah/lt3', 'AdminController@lttigaTambah')
            ->name('lttigaTambah');

        // Export into Excel, PDF
        Route::get('/dairi/excel', 'AdminController@excel')
            ->name('excel');

        // PDF Generate
        Route::get('/dairi/pdf', 'AdminController@pdfgenerate')
            ->name('pdf');
        Route::get('/dairi/print', 'AdminController@pdfprint')
            ->name('print');
        Route::get('/dairi/card/{id}', 'AdminController@card')
            ->name('card');
        Route::get('/dairi/setting', 'AdminController@setting')
            ->name('setting');
        Route::post('/dairi/setting/update', 'AdminController@changePass');
});


/////////////////////
// 04 DELI SERDANG //
/////////////////////
Route::namespace('AdminDeliserdang')
    ->name('deliserdang.')
    ->middleware(['auth', 'auth.deliserdang'])
    ->group(function() {
        Route::get('/deliserdang', 'AdminController@index')
            ->name('dashboard');
        Route::get('/deliserdang/all', 'AdminController@all')
            ->name('all');
        Route::get('/deliserdang/{id}/edit', 'AdminController@edit');
        Route::post('/deliserdang/add', 'AdminController@add')
            ->name('add');
        Route::post('/deliserdang/update', 'AdminController@update')
            ->name('update');
        Route::post('/deliserdang/destroy/{id}', 'AdminController@destroy');
        Route::post('/deliserdang/cancel/{id}/lt1', 'AdminController@cancelLt1');
        Route::post('/deliserdang/cancel/{id}/lt2', 'AdminController@cancelLt2');
        Route::post('/deliserdang/cancel/{id}/lt3', 'AdminController@cancelLt3');

        // LT 1, LT 2, LT 3
        Route::get('/deliserdang/lt1', 'AdminController@lt1')
            ->name('ltsatu');
        Route::get('/deliserdang/lt2', 'AdminController@lt2')
            ->name('ltdua');
        Route::get('/deliserdang/lt3', 'AdminController@lt3')
            ->name('lttiga');
        Route::get('/deliserdang/{id}/lt1', 'AdminController@ltsatu');
        Route::get('/deliserdang/{id}/lt2', 'AdminController@ltdua');
        Route::get('/deliserdang/{id}/lt3', 'AdminController@lttiga');
        Route::post('/deliserdang/tambah/lt1', 'AdminController@ltsatuTambah')
            ->name('ltsatuTambah');
        Route::post('/deliserdang/tambah/lt2', 'AdminController@ltduaTambah')
            ->name('ltduaTambah');
        Route::post('/deliserdang/tambah/lt3', 'AdminController@lttigaTambah')
            ->name('lttigaTambah');

        // Export into Excel, PDF
        Route::get('/deliserdang/excel', 'AdminController@excel')
            ->name('excel');

        // PDF Generate
        Route::get('/deliserdang/pdf', 'AdminController@pdfgenerate')
            ->name('pdf');
        Route::get('/deliserdang/print', 'AdminController@pdfprint')
            ->name('print');
        Route::get('/deliserdang/card/{id}', 'AdminController@card')
            ->name('card');
        Route::get('/deliserdang/setting', 'AdminController@setting')
            ->name('setting');
        Route::post('/deliserdang/setting/update', 'AdminController@changePass');
});


///////////////////////////
// 05 HUMBANG HASUNDUTAN //
///////////////////////////
Route::namespace('AdminHumbanghasundutan')->name('humbanghasundutan.')->middleware(['auth', 'auth.humbanghasundutan'])->group(function() {
    Route::get('/humbanghasundutan', 'AdminController@index')->name('dashboard');
    Route::get('/humbanghasundutan/all', 'AdminController@all')->name('all');
    Route::get('/humbanghasundutan/{id}/edit', 'AdminController@edit');
    Route::post('/humbanghasundutan/add', 'AdminController@add')->name('add');
    Route::post('/humbanghasundutan/update', 'AdminController@update')->name('update');
    Route::post('/humbanghasundutan/destroy/{id}', 'AdminController@destroy');
    Route::post('/humbanghasundutan/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/humbanghasundutan/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/humbanghasundutan/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/humbanghasundutan/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/humbanghasundutan/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/humbanghasundutan/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/humbanghasundutan/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/humbanghasundutan/{id}/lt2', 'AdminController@ltdua');
    Route::get('/humbanghasundutan/{id}/lt3', 'AdminController@lttiga');
    Route::post('/humbanghasundutan/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/humbanghasundutan/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/humbanghasundutan/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/humbanghasundutan/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/humbanghasundutan/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/humbanghasundutan/print', 'AdminController@pdfprint')->name('print');
    Route::get('/humbanghasundutan/card/{id}', 'AdminController@card')->name('card');
});


/////////////
// 06 KARO //
/////////////
Route::namespace('AdminKaro')->name('karo.')->middleware(['auth', 'auth.karo'])->group(function() {
    Route::get('/karo', 'AdminController@index')->name('dashboard');
    Route::get('/karo/all', 'AdminController@all')->name('all');
    Route::get('/karo/{id}/edit', 'AdminController@edit');
    Route::post('/karo/add', 'AdminController@add')->name('add');
    Route::post('/karo/update', 'AdminController@update')->name('update');
    Route::post('/karo/destroy/{id}', 'AdminController@destroy');
    Route::post('/karo/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/karo/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/karo/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/karo/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/karo/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/karo/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/karo/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/karo/{id}/lt2', 'AdminController@ltdua');
    Route::get('/karo/{id}/lt3', 'AdminController@lttiga');
    Route::post('/karo/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/karo/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/karo/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/karo/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/karo/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/karo/print', 'AdminController@pdfprint')->name('print');
    Route::get('/karo/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////
// 07 LABUHAN BATU //
/////////////////////
Route::namespace('AdminLabuhanbatu')->name('labuhanbatu.')->middleware(['auth', 'auth.labuhanbatu'])->group(function() {
    Route::get('/labuhanbatu', 'AdminController@index')->name('dashboard');
    Route::get('/labuhanbatu/all', 'AdminController@all')->name('all');
    Route::get('/labuhanbatu/{id}/edit', 'AdminController@edit');
    Route::post('/labuhanbatu/add', 'AdminController@add')->name('add');
    Route::post('/labuhanbatu/update', 'AdminController@update')->name('update');
    Route::post('/labuhanbatu/destroy/{id}', 'AdminController@destroy');
    Route::post('/labuhanbatu/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/labuhanbatu/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/labuhanbatu/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/labuhanbatu/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/labuhanbatu/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/labuhanbatu/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/labuhanbatu/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/labuhanbatu/{id}/lt2', 'AdminController@ltdua');
    Route::get('/labuhanbatu/{id}/lt3', 'AdminController@lttiga');
    Route::post('/labuhanbatu/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/labuhanbatu/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/labuhanbatu/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/labuhanbatu/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/labuhanbatu/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/labuhanbatu/print', 'AdminController@pdfprint')->name('print');
    Route::get('/labuhanbatu/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////////
// 08 LABUHAN BATU SELATAN //
/////////////////////////////
Route::namespace('AdminLabuhanbatuselatan')->name('labuhanbatuselatan.')->middleware(['auth', 'auth.labuhanbatuselatan'])->group(function() {
    Route::get('/labuhanbatuselatan', 'AdminController@index')->name('dashboard');
    Route::get('/labuhanbatuselatan/all', 'AdminController@all')->name('all');
    Route::get('/labuhanbatuselatan/{id}/edit', 'AdminController@edit');
    Route::post('/labuhanbatuselatan/add', 'AdminController@add')->name('add');
    Route::post('/labuhanbatuselatan/update', 'AdminController@update')->name('update');
    Route::post('/labuhanbatuselatan/destroy/{id}', 'AdminController@destroy');
    Route::post('/labuhanbatuselatan/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/labuhanbatuselatan/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/labuhanbatuselatan/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/labuhanbatuselatan/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/labuhanbatuselatan/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/labuhanbatuselatan/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/labuhanbatuselatan/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/labuhanbatuselatan/{id}/lt2', 'AdminController@ltdua');
    Route::get('/labuhanbatuselatan/{id}/lt3', 'AdminController@lttiga');
    Route::post('/labuhanbatuselatan/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/labuhanbatuselatan/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/labuhanbatuselatan/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/labuhanbatuselatan/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/labuhanbatuselatan/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/labuhanbatuselatan/print', 'AdminController@pdfprint')->name('print');
    Route::get('/labuhanbatuselatan/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////////
// 09 LABUHAN BATU UTARA //
/////////////////////////////
Route::namespace('AdminLabuhanbatuutara')->name('labuhanbatuutara.')->middleware(['auth', 'auth.labuhanbatuutara'])->group(function() {
    Route::get('/labuhanbatuutara', 'AdminController@index')->name('dashboard');
    Route::get('/labuhanbatuutara/all', 'AdminController@all')->name('all');
    Route::get('/labuhanbatuutara/{id}/edit', 'AdminController@edit');
    Route::post('/labuhanbatuutara/add', 'AdminController@add')->name('add');
    Route::post('/labuhanbatuutara/update', 'AdminController@update')->name('update');
    Route::post('/labuhanbatuutara/destroy/{id}', 'AdminController@destroy');
    Route::post('/labuhanbatuutara/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/labuhanbatuutara/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/labuhanbatuutara/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/labuhanbatuutara/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/labuhanbatuutara/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/labuhanbatuutara/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/labuhanbatuutara/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/labuhanbatuutara/{id}/lt2', 'AdminController@ltdua');
    Route::get('/labuhanbatuutara/{id}/lt3', 'AdminController@lttiga');
    Route::post('/labuhanbatuutara/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/labuhanbatuutara/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/labuhanbatuutara/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/labuhanbatuutara/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/labuhanbatuutara/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/labuhanbatuutara/print', 'AdminController@pdfprint')->name('print');
    Route::get('/labuhanbatuutara/card/{id}', 'AdminController@card')->name('card');
});


////////////////
// 10 LANGKAT //
////////////////
Route::namespace('AdminLangkat')->name('langkat.')->middleware(['auth', 'auth.langkat'])->group(function() {
    Route::get('/langkat', 'AdminController@index')->name('dashboard');
    Route::get('/langkat/all', 'AdminController@all')->name('all');
    Route::get('/langkat/{id}/edit', 'AdminController@edit');
    Route::post('/langkat/add', 'AdminController@add')->name('add');
    Route::post('/langkat/update', 'AdminController@update')->name('update');
    Route::post('/langkat/destroy/{id}', 'AdminController@destroy');
    Route::post('/langkat/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/langkat/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/langkat/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/langkat/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/langkat/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/langkat/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/langkat/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/langkat/{id}/lt2', 'AdminController@ltdua');
    Route::get('/langkat/{id}/lt3', 'AdminController@lttiga');
    Route::post('/langkat/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/langkat/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/langkat/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/langkat/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/langkat/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/langkat/print', 'AdminController@pdfprint')->name('print');
    Route::get('/langkat/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////
// 11 MANDAILING NATAL //
/////////////////////////
Route::namespace('AdminMandailingnatal')->name('mandailingnatal.')->middleware(['auth', 'auth.mandailingnatal'])->group(function() {
    Route::get('/mandailingnatal', 'AdminController@index')->name('dashboard');
    Route::get('/mandailingnatal/all', 'AdminController@all')->name('all');
    Route::get('/mandailingnatal/{id}/edit', 'AdminController@edit');
    Route::post('/mandailingnatal/add', 'AdminController@add')->name('add');
    Route::post('/mandailingnatal/update', 'AdminController@update')->name('update');
    Route::post('/mandailingnatal/destroy/{id}', 'AdminController@destroy');
    Route::post('/mandailingnatal/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/mandailingnatal/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/mandailingnatal/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/mandailingnatal/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/mandailingnatal/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/mandailingnatal/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/mandailingnatal/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/mandailingnatal/{id}/lt2', 'AdminController@ltdua');
    Route::get('/mandailingnatal/{id}/lt3', 'AdminController@lttiga');
    Route::post('/mandailingnatal/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/mandailingnatal/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/mandailingnatal/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/mandailingnatal/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/mandailingnatal/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/mandailingnatal/print', 'AdminController@pdfprint')->name('print');
    Route::get('/mandailingnatal/card/{id}', 'AdminController@card')->name('card');
});


/////////////
// 12 NIAS //
/////////////
Route::namespace('AdminNias')->name('nias.')->middleware(['auth', 'auth.nias'])->group(function() {
    Route::get('/nias', 'AdminController@index')->name('dashboard');
    Route::get('/nias/all', 'AdminController@all')->name('all');
    Route::get('/nias/{id}/edit', 'AdminController@edit');
    Route::post('/nias/add', 'AdminController@add')->name('add');
    Route::post('/nias/update', 'AdminController@update')->name('update');
    Route::post('/nias/destroy/{id}', 'AdminController@destroy');
    Route::post('/nias/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/nias/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/nias/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/nias/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/nias/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/nias/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/nias/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/nias/{id}/lt2', 'AdminController@ltdua');
    Route::get('/nias/{id}/lt3', 'AdminController@lttiga');
    Route::post('/nias/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/nias/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/nias/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/nias/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/nias/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/nias/print', 'AdminController@pdfprint')->name('print');
    Route::get('/nias/card/{id}', 'AdminController@card')->name('card');
});


///////////////////
// 13 NIAS BARAT //
///////////////////
Route::namespace('AdminNiasbarat')->name('niasbarat.')->middleware(['auth', 'auth.niasbarat'])->group(function() {
    Route::get('/niasbarat', 'AdminController@index')->name('dashboard');
    Route::get('/niasbarat/all', 'AdminController@all')->name('all');
    Route::get('/niasbarat/{id}/edit', 'AdminController@edit');
    Route::post('/niasbarat/add', 'AdminController@add')->name('add');
    Route::post('/niasbarat/update', 'AdminController@update')->name('update');
    Route::post('/niasbarat/destroy/{id}', 'AdminController@destroy');
    Route::post('/niasbarat/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/niasbarat/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/niasbarat/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/niasbarat/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/niasbarat/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/niasbarat/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/niasbarat/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/niasbarat/{id}/lt2', 'AdminController@ltdua');
    Route::get('/niasbarat/{id}/lt3', 'AdminController@lttiga');
    Route::post('/niasbarat/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/niasbarat/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/niasbarat/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/niasbarat/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/niasbarat/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/niasbarat/print', 'AdminController@pdfprint')->name('print');
    Route::get('/niasbarat/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////
// 14 NIAS SELATAN //
/////////////////////
Route::namespace('AdminNiasselatan')->name('niasselatan.')->middleware(['auth', 'auth.niasselatan'])->group(function() {
    Route::get('/niasselatan', 'AdminController@index')->name('dashboard');
    Route::get('/niasselatan/all', 'AdminController@all')->name('all');
    Route::get('/niasselatan/{id}/edit', 'AdminController@edit');
    Route::post('/niasselatan/add', 'AdminController@add')->name('add');
    Route::post('/niasselatan/update', 'AdminController@update')->name('update');
    Route::post('/niasselatan/destroy/{id}', 'AdminController@destroy');
    Route::post('/niasselatan/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/niasselatan/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/niasselatan/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/niasselatan/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/niasselatan/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/niasselatan/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/niasselatan/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/niasselatan/{id}/lt2', 'AdminController@ltdua');
    Route::get('/niasselatan/{id}/lt3', 'AdminController@lttiga');
    Route::post('/niasselatan/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/niasselatan/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/niasselatan/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/niasselatan/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/niasselatan/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/niasselatan/print', 'AdminController@pdfprint')->name('print');
    Route::get('/niasselatan/card/{id}', 'AdminController@card')->name('card');
});


///////////////////
// 15 NIAS UTARA //
///////////////////
Route::namespace('AdminNiasutara')->name('niasutara.')->middleware(['auth', 'auth.niasutara'])->group(function() {
    Route::get('/niasutara', 'AdminController@index')->name('dashboard');
    Route::get('/niasutara/all', 'AdminController@all')->name('all');
    Route::get('/niasutara/{id}/edit', 'AdminController@edit');
    Route::post('/niasutara/add', 'AdminController@add')->name('add');
    Route::post('/niasutara/update', 'AdminController@update')->name('update');
    Route::post('/niasutara/destroy/{id}', 'AdminController@destroy');
    Route::post('/niasutara/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/niasutara/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/niasutara/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/niasutara/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/niasutara/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/niasutara/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/niasutara/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/niasutara/{id}/lt2', 'AdminController@ltdua');
    Route::get('/niasutara/{id}/lt3', 'AdminController@lttiga');
    Route::post('/niasutara/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/niasutara/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/niasutara/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/niasutara/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/niasutara/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/niasutara/print', 'AdminController@pdfprint')->name('print');
    Route::get('/niasutara/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////
// 16 PADANG LAWAS //
/////////////////////
Route::namespace('AdminPadanglawas')->name('padanglawas.')->middleware(['auth', 'auth.padanglawas'])->group(function() {
    Route::get('/padanglawas', 'AdminController@index')->name('dashboard');
    Route::get('/padanglawas/all', 'AdminController@all')->name('all');
    Route::get('/padanglawas/{id}/edit', 'AdminController@edit');
    Route::post('/padanglawas/add', 'AdminController@add')->name('add');
    Route::post('/padanglawas/update', 'AdminController@update')->name('update');
    Route::post('/padanglawas/destroy/{id}', 'AdminController@destroy');
    Route::post('/padanglawas/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/padanglawas/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/padanglawas/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/padanglawas/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/padanglawas/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/padanglawas/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/padanglawas/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/padanglawas/{id}/lt2', 'AdminController@ltdua');
    Route::get('/padanglawas/{id}/lt3', 'AdminController@lttiga');
    Route::post('/padanglawas/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/padanglawas/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/padanglawas/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/padanglawas/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/padanglawas/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/padanglawas/print', 'AdminController@pdfprint')->name('print');
    Route::get('/padanglawas/card/{id}', 'AdminController@card')->name('card');
});


///////////////////////////
// 17 PADANG LAWAS UTARA //
///////////////////////////
Route::namespace('AdminPadanglawasutara')->name('padanglawasutara.')->middleware(['auth', 'auth.padanglawasutara'])->group(function() {
    Route::get('/padanglawasutara', 'AdminController@index')->name('dashboard');
    Route::get('/padanglawasutara/all', 'AdminController@all')->name('all');
    Route::get('/padanglawasutara/{id}/edit', 'AdminController@edit');
    Route::post('/padanglawasutara/add', 'AdminController@add')->name('add');
    Route::post('/padanglawasutara/update', 'AdminController@update')->name('update');
    Route::post('/padanglawasutara/destroy/{id}', 'AdminController@destroy');
    Route::post('/padanglawasutara/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/padanglawasutara/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/padanglawasutara/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/padanglawasutara/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/padanglawasutara/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/padanglawasutara/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/padanglawasutara/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/padanglawasutara/{id}/lt2', 'AdminController@ltdua');
    Route::get('/padanglawasutara/{id}/lt3', 'AdminController@lttiga');
    Route::post('/padanglawasutara/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/padanglawasutara/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/padanglawasutara/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/padanglawasutara/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/padanglawasutara/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/padanglawasutara/print', 'AdminController@pdfprint')->name('print');
    Route::get('/padanglawasutara/card/{id}', 'AdminController@card')->name('card');
});


//////////////////////
// 18 PAKPAK BHARAT //
//////////////////////
Route::namespace('AdminPakpakbharat')->name('pakpakbharat.')->middleware(['auth', 'auth.pakpakbharat'])->group(function() {
    Route::get('/pakpakbharat', 'AdminController@index')->name('dashboard');
    Route::get('/pakpakbharat/all', 'AdminController@all')->name('all');
    Route::get('/pakpakbharat/{id}/edit', 'AdminController@edit');
    Route::post('/pakpakbharat/add', 'AdminController@add')->name('add');
    Route::post('/pakpakbharat/update', 'AdminController@update')->name('update');
    Route::post('/pakpakbharat/destroy/{id}', 'AdminController@destroy');
    Route::post('/pakpakbharat/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/pakpakbharat/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/pakpakbharat/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/pakpakbharat/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/pakpakbharat/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/pakpakbharat/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/pakpakbharat/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/pakpakbharat/{id}/lt2', 'AdminController@ltdua');
    Route::get('/pakpakbharat/{id}/lt3', 'AdminController@lttiga');
    Route::post('/pakpakbharat/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/pakpakbharat/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/pakpakbharat/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/pakpakbharat/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/pakpakbharat/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/pakpakbharat/print', 'AdminController@pdfprint')->name('print');
    Route::get('/pakpakbharat/card/{id}', 'AdminController@card')->name('card');
});


////////////////
// 19 SAMOSIR //
////////////////
Route::namespace('AdminSamosir')->name('samosir.')->middleware(['auth', 'auth.samosir'])->group(function() {
    Route::get('/samosir', 'AdminController@index')->name('dashboard');
    Route::get('/samosir/all', 'AdminController@all')->name('all');
    Route::get('/samosir/{id}/edit', 'AdminController@edit');
    Route::post('/samosir/add', 'AdminController@add')->name('add');
    Route::post('/samosir/update', 'AdminController@update')->name('update');
    Route::post('/samosir/destroy/{id}', 'AdminController@destroy');
    Route::post('/samosir/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/samosir/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/samosir/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/samosir/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/samosir/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/samosir/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/samosir/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/samosir/{id}/lt2', 'AdminController@ltdua');
    Route::get('/samosir/{id}/lt3', 'AdminController@lttiga');
    Route::post('/samosir/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/samosir/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/samosir/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/samosir/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/samosir/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/samosir/print', 'AdminController@pdfprint')->name('print');
    Route::get('/samosir/card/{id}', 'AdminController@card')->name('card');
});


////////////////////////
// 20 SERDANG BEDAGAI //
////////////////////////
Route::namespace('AdminSerdangbedagai')->name('serdangbedagai.')->middleware(['auth', 'auth.serdangbedagai'])->group(function() {
    Route::get('/serdangbedagai', 'AdminController@index')->name('dashboard');
    Route::get('/serdangbedagai/all', 'AdminController@all')->name('all');
    Route::get('/serdangbedagai/{id}/edit', 'AdminController@edit');
    Route::post('/serdangbedagai/add', 'AdminController@add')->name('add');
    Route::post('/serdangbedagai/update', 'AdminController@update')->name('update');
    Route::post('/serdangbedagai/destroy/{id}', 'AdminController@destroy');
    Route::post('/serdangbedagai/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/serdangbedagai/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/serdangbedagai/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/serdangbedagai/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/serdangbedagai/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/serdangbedagai/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/serdangbedagai/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/serdangbedagai/{id}/lt2', 'AdminController@ltdua');
    Route::get('/serdangbedagai/{id}/lt3', 'AdminController@lttiga');
    Route::post('/serdangbedagai/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/serdangbedagai/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/serdangbedagai/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/serdangbedagai/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/serdangbedagai/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/serdangbedagai/print', 'AdminController@pdfprint')->name('print');
    Route::get('/serdangbedagai/card/{id}', 'AdminController@card')->name('card');
});


////////////////////////
// 21 SIMALUNGUN //
////////////////////////
Route::namespace('AdminSimalungun')->name('simalungun.')->middleware(['auth', 'auth.simalungun'])->group(function() {
    Route::get('/simalungun', 'AdminController@index')->name('dashboard');
    Route::get('/simalungun/all', 'AdminController@all')->name('all');
    Route::get('/simalungun/{id}/edit', 'AdminController@edit');
    Route::post('/simalungun/add', 'AdminController@add')->name('add');
    Route::post('/simalungun/update', 'AdminController@update')->name('update');
    Route::post('/simalungun/destroy/{id}', 'AdminController@destroy');
    Route::post('/simalungun/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/simalungun/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/simalungun/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/simalungun/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/simalungun/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/simalungun/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/simalungun/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/simalungun/{id}/lt2', 'AdminController@ltdua');
    Route::get('/simalungun/{id}/lt3', 'AdminController@lttiga');
    Route::post('/simalungun/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/simalungun/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/simalungun/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/simalungun/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/simalungun/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/simalungun/print', 'AdminController@pdfprint')->name('print');
    Route::get('/simalungun/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////
// 22 TAPANULI SELATAN //
/////////////////////////
Route::namespace('AdminTapanuliselatan')->name('tapanuliselatan.')->middleware(['auth', 'auth.tapanuliselatan'])->group(function() {
    Route::get('/tapanuliselatan', 'AdminController@index')->name('dashboard');
    Route::get('/tapanuliselatan/all', 'AdminController@all')->name('all');
    Route::get('/tapanuliselatan/{id}/edit', 'AdminController@edit');
    Route::post('/tapanuliselatan/add', 'AdminController@add')->name('add');
    Route::post('/tapanuliselatan/update', 'AdminController@update')->name('update');
    Route::post('/tapanuliselatan/destroy/{id}', 'AdminController@destroy');
    Route::post('/tapanuliselatan/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/tapanuliselatan/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/tapanuliselatan/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/tapanuliselatan/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/tapanuliselatan/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/tapanuliselatan/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/tapanuliselatan/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/tapanuliselatan/{id}/lt2', 'AdminController@ltdua');
    Route::get('/tapanuliselatan/{id}/lt3', 'AdminController@lttiga');
    Route::post('/tapanuliselatan/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/tapanuliselatan/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/tapanuliselatan/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/tapanuliselatan/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/tapanuliselatan/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/tapanuliselatan/print', 'AdminController@pdfprint')->name('print');
    Route::get('/tapanuliselatan/card/{id}', 'AdminController@card')->name('card');
});


////////////////////////
// 23 TAPANULI TENGAH //
////////////////////////
Route::namespace('AdminTapanulitengah')->name('tapanulitengah.')->middleware(['auth', 'auth.tapanulitengah'])->group(function() {
    Route::get('/tapanulitengah', 'AdminController@index')->name('dashboard');
    Route::get('/tapanulitengah/all', 'AdminController@all')->name('all');
    Route::get('/tapanulitengah/{id}/edit', 'AdminController@edit');
    Route::post('/tapanulitengah/add', 'AdminController@add')->name('add');
    Route::post('/tapanulitengah/update', 'AdminController@update')->name('update');
    Route::post('/tapanulitengah/destroy/{id}', 'AdminController@destroy');
    Route::post('/tapanulitengah/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/tapanulitengah/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/tapanulitengah/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/tapanulitengah/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/tapanulitengah/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/tapanulitengah/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/tapanulitengah/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/tapanulitengah/{id}/lt2', 'AdminController@ltdua');
    Route::get('/tapanulitengah/{id}/lt3', 'AdminController@lttiga');
    Route::post('/tapanulitengah/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/tapanulitengah/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/tapanulitengah/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/tapanulitengah/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/tapanulitengah/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/tapanulitengah/print', 'AdminController@pdfprint')->name('print');
    Route::get('/tapanulitengah/card/{id}', 'AdminController@card')->name('card');
});


///////////////////////
// 24 TAPANULI UTARA //
///////////////////////
Route::namespace('AdminTapanuliutara')->name('tapanuliutara.')->middleware(['auth', 'auth.tapanuliutara'])->group(function() {
    Route::get('/tapanuliutara', 'AdminController@index')->name('dashboard');
    Route::get('/tapanuliutara/all', 'AdminController@all')->name('all');
    Route::get('/tapanuliutara/{id}/edit', 'AdminController@edit');
    Route::post('/tapanuliutara/add', 'AdminController@add')->name('add');
    Route::post('/tapanuliutara/update', 'AdminController@update')->name('update');
    Route::post('/tapanuliutara/destroy/{id}', 'AdminController@destroy');
    Route::post('/tapanuliutara/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/tapanuliutara/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/tapanuliutara/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/tapanuliutara/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/tapanuliutara/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/tapanuliutara/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/tapanuliutara/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/tapanuliutara/{id}/lt2', 'AdminController@ltdua');
    Route::get('/tapanuliutara/{id}/lt3', 'AdminController@lttiga');
    Route::post('/tapanuliutara/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/tapanuliutara/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/tapanuliutara/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/tapanuliutara/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/tapanuliutara/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/tapanuliutara/print', 'AdminController@pdfprint')->name('print');
    Route::get('/tapanuliutara/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////
// 25 TOBA SAMOSIR //
/////////////////////
Route::namespace('AdminTobasamosir')->name('tobasamosir.')->middleware(['auth', 'auth.tobasamosir'])->group(function() {
    Route::get('/tobasamosir', 'AdminController@index')->name('dashboard');
    Route::get('/tobasamosir/all', 'AdminController@all')->name('all');
    Route::get('/tobasamosir/{id}/edit', 'AdminController@edit');
    Route::post('/tobasamosir/add', 'AdminController@add')->name('add');
    Route::post('/tobasamosir/update', 'AdminController@update')->name('update');
    Route::post('/tobasamosir/destroy/{id}', 'AdminController@destroy');
    Route::post('/tobasamosir/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/tobasamosir/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/tobasamosir/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/tobasamosir/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/tobasamosir/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/tobasamosir/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/tobasamosir/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/tobasamosir/{id}/lt2', 'AdminController@ltdua');
    Route::get('/tobasamosir/{id}/lt3', 'AdminController@lttiga');
    Route::post('/tobasamosir/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/tobasamosir/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/tobasamosir/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/tobasamosir/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/tobasamosir/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/tobasamosir/print', 'AdminController@pdfprint')->name('print');
    Route::get('/tobasamosir/card/{id}', 'AdminController@card')->name('card');
});


///////////////
// 26 BINJAI //
///////////////
Route::namespace('AdminBinjai')
    ->name('binjai.')
    ->middleware(['auth', 'auth.binjai'])
    ->group(function() {
        Route::get('/binjai', 'AdminController@index')
            ->name('dashboard');
        Route::get('/binjai/all', 'AdminController@all')
            ->name('all');
        Route::get('/binjai/{id}/edit', 'AdminController@edit');
        Route::post('/binjai/add', 'AdminController@add')
            ->name('add');
        Route::post('/binjai/update', 'AdminController@update')
            ->name('update');
        Route::post('/binjai/destroy/{id}', 'AdminController@destroy');
        Route::post('/binjai/cancel/{id}/lt1', 'AdminController@cancelLt1');
        Route::post('/binjai/cancel/{id}/lt2', 'AdminController@cancelLt2');
        Route::post('/binjai/cancel/{id}/lt3', 'AdminController@cancelLt3');

        // LT 1, LT 2, LT 3
        Route::get('/binjai/lt1', 'AdminController@lt1')
            ->name('ltsatu');
        Route::get('/binjai/lt2', 'AdminController@lt2')
            ->name('ltdua');
        Route::get('/binjai/lt3', 'AdminController@lt3')
            ->name('lttiga');
        Route::get('/binjai/{id}/lt1', 'AdminController@ltsatu');
        Route::get('/binjai/{id}/lt2', 'AdminController@ltdua');
        Route::get('/binjai/{id}/lt3', 'AdminController@lttiga');
        Route::post('/binjai/tambah/lt1', 'AdminController@ltsatuTambah')
            ->name('ltsatuTambah');
        Route::post('/binjai/tambah/lt2', 'AdminController@ltduaTambah')
            ->name('ltduaTambah');
        Route::post('/binjai/tambah/lt3', 'AdminController@lttigaTambah')
            ->name('lttigaTambah');

        // Export into Excel, PDF
        Route::get('/binjai/excel', 'AdminController@excel')
            ->name('excel');

        // PDF Generate
        Route::get('/binjai/pdf', 'AdminController@pdfgenerate')
            ->name('pdf');
        Route::get('/binjai/print', 'AdminController@pdfprint')
            ->name('print');
        Route::get('/binjai/card/{id}', 'AdminController@card')
            ->name('card');
        Route::get('/binjai/setting', 'AdminController@setting')
            ->name('setting');
        Route::post('/binjai/setting/update', 'AdminController@changePass');
});


/////////////////////
// 27 GUNUNGSITOLI //
/////////////////////
Route::namespace('AdminGunungsitoli')->name('gunungsitoli.')->middleware(['auth', 'auth.gunungsitoli'])->group(function() {
    Route::get('/gunungsitoli', 'AdminController@index')->name('dashboard');
    Route::get('/gunungsitoli/all', 'AdminController@all')->name('all');
    Route::get('/gunungsitoli/{id}/edit', 'AdminController@edit');
    Route::post('/gunungsitoli/add', 'AdminController@add')->name('add');
    Route::post('/gunungsitoli/update', 'AdminController@update')->name('update');
    Route::post('/gunungsitoli/destroy/{id}', 'AdminController@destroy');
    Route::post('/gunungsitoli/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/gunungsitoli/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/gunungsitoli/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/gunungsitoli/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/gunungsitoli/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/gunungsitoli/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/gunungsitoli/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/gunungsitoli/{id}/lt2', 'AdminController@ltdua');
    Route::get('/gunungsitoli/{id}/lt3', 'AdminController@lttiga');
    Route::post('/gunungsitoli/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/gunungsitoli/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/gunungsitoli/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/gunungsitoli/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/gunungsitoli/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/gunungsitoli/print', 'AdminController@pdfprint')->name('print');
    Route::get('/gunungsitoli/card/{id}', 'AdminController@card')->name('card');
});


//////////////
// 28 MEDAN //
//////////////
Route::namespace('AdminMedan')->name('medan.')->middleware(['auth', 'auth.medan'])->group(function() {
    Route::get('/medan', 'AdminController@index')->name('dashboard');
    Route::get('/medan/all', 'AdminController@all')->name('all');
    Route::get('/medan/{id}/edit', 'AdminController@edit');
    Route::post('/medan/add', 'AdminController@add')->name('add');
    Route::post('/medan/update', 'AdminController@update')->name('update');
    Route::post('/medan/destroy/{id}', 'AdminController@destroy');
    Route::post('/medan/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/medan/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/medan/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/medan/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/medan/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/medan/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/medan/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/medan/{id}/lt2', 'AdminController@ltdua');
    Route::get('/medan/{id}/lt3', 'AdminController@lttiga');
    Route::post('/medan/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/medan/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/medan/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/medan/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/medan/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/medan/print', 'AdminController@pdfprint')->name('print');
    Route::get('/medan/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////
// 29 PADANGSIDEMPUAN //
////////////////////////
Route::namespace('AdminPadangsidempuan')->name('padangsidempuan.')->middleware(['auth', 'auth.padangsidempuan'])->group(function() {
    // View
    Route::get('/padangsidempuan', 'AdminController@index')->name('dashboard');
    Route::get('/padangsidempuan/all', 'AdminController@all')->name('all');
    Route::get('/padangsidempuan/{id}/edit', 'AdminController@edit');
    Route::post('/padangsidempuan/add', 'AdminController@add')->name('add');
    Route::post('/padangsidempuan/update', 'AdminController@update')->name('update');
    Route::post('/padangsidempuan/destroy/{id}', 'AdminController@destroy');
    Route::post('/padangsidempuan/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/padangsidempuan/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/padangsidempuan/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/padangsidempuan/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/padangsidempuan/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/padangsidempuan/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/padangsidempuan/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/padangsidempuan/{id}/lt2', 'AdminController@ltdua');
    Route::get('/padangsidempuan/{id}/lt3', 'AdminController@lttiga');
    Route::post('/padangsidempuan/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/padangsidempuan/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/padangsidempuan/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/padangsidempuan/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/padangsidempuan/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/padangsidempuan/print', 'AdminController@pdfprint')->name('print');
    Route::get('/padangsidempuan/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////
// 30 PEMATANGSIANTAR //
////////////////////////
Route::namespace('AdminPematangsiantar')->name('pematangsiantar.')->middleware(['auth', 'auth.pematangsiantar'])->group(function() {
    // View
    Route::get('/pematangsiantar', 'AdminController@index')->name('dashboard');
    Route::get('/pematangsiantar/all', 'AdminController@all')->name('all');
    Route::get('/pematangsiantar/{id}/edit', 'AdminController@edit');
    Route::post('/pematangsiantar/add', 'AdminController@add')->name('add');
    Route::post('/pematangsiantar/update', 'AdminController@update')->name('update');
    Route::post('/pematangsiantar/destroy/{id}', 'AdminController@destroy');
    Route::post('/pematangsiantar/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/pematangsiantar/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/pematangsiantar/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/pematangsiantar/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/pematangsiantar/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/pematangsiantar/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/pematangsiantar/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/pematangsiantar/{id}/lt2', 'AdminController@ltdua');
    Route::get('/pematangsiantar/{id}/lt3', 'AdminController@lttiga');
    Route::post('/pematangsiantar/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/pematangsiantar/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/pematangsiantar/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/pematangsiantar/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/pematangsiantar/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/pematangsiantar/print', 'AdminController@pdfprint')->name('print');
    Route::get('/pematangsiantar/card/{id}', 'AdminController@card')->name('card');
});


/////////////////////////
// 31 SIBOLGA //
////////////////////////
Route::namespace('AdminSibolga')->name('sibolga.')->middleware(['auth', 'auth.sibolga'])->group(function() {
    // View
    Route::get('/sibolga', 'AdminController@index')->name('dashboard');
    Route::get('/sibolga/all', 'AdminController@all')->name('all');
    Route::get('/sibolga/{id}/edit', 'AdminController@edit');
    Route::post('/sibolga/add', 'AdminController@add')->name('add');
    Route::post('/sibolga/update', 'AdminController@update')->name('update');
    Route::post('/sibolga/destroy/{id}', 'AdminController@destroy');
    Route::post('/sibolga/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/sibolga/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/sibolga/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/sibolga/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/sibolga/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/sibolga/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/sibolga/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/sibolga/{id}/lt2', 'AdminController@ltdua');
    Route::get('/sibolga/{id}/lt3', 'AdminController@lttiga');
    Route::post('/sibolga/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/sibolga/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/sibolga/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/sibolga/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/sibolga/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/sibolga/print', 'AdminController@pdfprint')->name('print');
    Route::get('/sibolga/card/{id}', 'AdminController@card')->name('card');
});


////////////////////
// 32 ANJUNGBALAI //
////////////////////
Route::namespace('AdminTanjungbalai')->name('tanjungbalai.')->middleware(['auth', 'auth.tanjungbalai'])->group(function() {
    Route::get('/tanjungbalai', 'AdminController@index')->name('dashboard');
    Route::get('/tanjungbalai/all', 'AdminController@all')->name('all');
    Route::get('/tanjungbalai/{id}/edit', 'AdminController@edit');
    Route::post('/tanjungbalai/add', 'AdminController@add')->name('add');
    Route::post('/tanjungbalai/update', 'AdminController@update')->name('update');
    Route::post('/tanjungbalai/destroy/{id}', 'AdminController@destroy');
    Route::post('/tanjungbalai/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/tanjungbalai/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/tanjungbalai/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/tanjungbalai/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/tanjungbalai/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/tanjungbalai/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/tanjungbalai/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/tanjungbalai/{id}/lt2', 'AdminController@ltdua');
    Route::get('/tanjungbalai/{id}/lt3', 'AdminController@lttiga');
    Route::post('/tanjungbalai/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/tanjungbalai/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/tanjungbalai/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/tanjungbalai/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/tanjungbalai/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/tanjungbalai/print', 'AdminController@pdfprint')->name('print');
    Route::get('/tanjungbalai/card/{id}', 'AdminController@card')->name('card');
});


//////////////////////
// 33 TEBING TINGGI //
//////////////////////
Route::namespace('AdminTebingtinggi')->name('tebingtinggi.')->middleware(['auth', 'auth.tebingtinggi'])->group(function() {
    Route::get('/tebingtinggi', 'AdminController@index')->name('dashboard');
    Route::get('/tebingtinggi/all', 'AdminController@all')->name('all');
    Route::get('/tebingtinggi/{id}/edit', 'AdminController@edit');
    Route::post('/tebingtinggi/add', 'AdminController@add')->name('add');
    Route::post('/tebingtinggi/update', 'AdminController@update')->name('update');
    Route::post('/tebingtinggi/destroy/{id}', 'AdminController@destroy');
    Route::post('/tebingtinggi/cancel/{id}/lt1', 'AdminController@cancelLt1');
    Route::post('/tebingtinggi/cancel/{id}/lt2', 'AdminController@cancelLt2');
    Route::post('/tebingtinggi/cancel/{id}/lt3', 'AdminController@cancelLt3');

    // LT 1, LT 2, LT 3
    Route::get('/tebingtinggi/lt1', 'AdminController@lt1')->name('ltsatu');
    Route::get('/tebingtinggi/lt2', 'AdminController@lt2')->name('ltdua');
    Route::get('/tebingtinggi/lt3', 'AdminController@lt3')->name('lttiga');
    Route::get('/tebingtinggi/{id}/lt1', 'AdminController@ltsatu');
    Route::get('/tebingtinggi/{id}/lt2', 'AdminController@ltdua');
    Route::get('/tebingtinggi/{id}/lt3', 'AdminController@lttiga');
    Route::post('/tebingtinggi/tambah/lt1', 'AdminController@ltsatuTambah')->name('ltsatuTambah');
    Route::post('/tebingtinggi/tambah/lt2', 'AdminController@ltduaTambah')->name('ltduaTambah');
    Route::post('/tebingtinggi/tambah/lt3', 'AdminController@lttigaTambah')->name('lttigaTambah');

    // Export into Excel, PDF
    Route::get('/tebingtinggi/excel', 'AdminController@excel')->name('excel');

    // PDF Generate
    Route::get('/tebingtinggi/pdf', 'AdminController@pdfgenerate')->name('pdf');
    Route::get('/tebingtinggi/print', 'AdminController@pdfprint')->name('print');
    Route::get('/tebingtinggi/card/{id}', 'AdminController@card')->name('card');
});
