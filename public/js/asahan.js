$(document).ready(function () {
    $('#pass').on('click', function () {
        $('#formPass').modal('show');
    });

    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    $('#sample').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: "/asahan/setting/update",
            method: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        type: 'success',
                        title: 'Berhasil!',
                        text: 'Password berhasil diganti',
                    }).then(function () {
                        location.reload();
                    });
                    $('#formPass').modal('hide');
                }
            }
        });
    });
});